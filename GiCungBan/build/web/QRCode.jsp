<%-- 
    Document   : QRCode
    Created on : Mar 12, 2024, 9:18:56 PM
    Author     : PC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Nạp tiền</title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet"/>
        <!-- Custom styles for this template -->
        <link rel="icon" type="image/png" href="images/logo.png">
        <link href="css/Home.css" rel="stylesheet" />
        <link href="css/jumbotron-narrow.css" rel="stylesheet">      
        <script src="css/jquery-1.11.3.min.js"></script>
        
    </head>
    <body>
        <div class="container">
            <div class="header clearfix">

                <h3 class="text-muted">NẠP TIỀN</h3>
            </div>
            <div class="table-responsive">
                <form>        
                    <img src="images/QRcode.jpg" alt="Mô tả ảnh"> 
                    <div style="color: red; font-weight: bold;">
                        SAU KHI QUAY VỀ TRANG CHỦ, VUI LÒNG ĐỢI TRONG GIÂY LÁT ĐỂ TIỀN ĐƯỢC NẠP VÀO TÀI KHOẢN !
                    </div>

                    <a href="vnpay_return.jsp" class="btn btn-default">Hủy</a>                   

                    <a href="home" class="btn">Về trang chủ</a>

                </form>

            </div>
        </div>
    </body>
</html>
