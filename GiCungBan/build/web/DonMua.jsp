<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>GiCungBan</title>
        <link rel="icon" type="image/png" href="images/logo.png">
        <!-- Bootstrap CSS link -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/Home.css" rel="stylesheet" />
        <link rel="icon" type="image/png" href="images/logo.png">
        <link href="css/DonBan.css" rel="stylesheet" />
        <style>
            table{
                border-collapse: collapse
            }
            .content{
                margin: auto;
                width: 80%;
                float: left;
                margin-left: 30px;
                height: auto
            }
            .pagination {
                display: inline-block;
            }
            .pagination a {
                color: black;
                font-size: 22px;
                float: left;
                padding: 8px 16px;
                text-decoration: none;
            }
            .pagination a.active {
                background-color: #333;
                color: white;
            }
            .pagination a:hover:not(.active) {
                background-color: chocolate;
            </style>
        </head>
        <body>

            <header>
                <div class="header-left">
                </div>
                <div class="header-center">
                    <h1>GiCungBan</h1>
                </div>
                <div class="header-right">
                </div>
            </header>

            <nav>
                <a class="text-decoration-none" href="home">Publicly traded market</a>
                <a class="text-decoration-none" href="vnpay_pay.jsp">Payment management</a>
                <a class="text-decoration-none" href="manage">My sales order</a>
                <a class="text-decoration-none" href="DonMua">My purchase order</a>
                <c:if test="${sessionScope.acc !=null}" >
                    <a class="text-decoration-none" href="profile" class="">Hello ${sessionScope.acc.username}</a>
                </c:if>
                <c:if test="${sessionScope.acc == null}" >
                    <a href="login.jsp" class="nav-item nav-link">Login</a>                               
                </c:if>
            </nav>

            <div class="container mt-4">
                <div class="order mb-4">
                    <div class="order-details">

                        <c:if test="${param.success ne null}">
                            <div class="alert alert-success" role="alert">
                                Success!
                            </div>
                        </c:if>

                        <form action="searchByOrderId" method="post" class="search-form">
                            <input type="text" name="keywordOrder" placeholder="Tìm kiếm tên sản phẩm" class="search-input">
                            <input type="hidden" name="id" value="${sessionScope.acc.uid}">
                            <button type="submit" class="search-btn">Search</button>
                        </form>

                        <c:if test="${empty listOrder}">
                            <p style="color: red;">Product not found!</p>
                        </c:if>
                        <br/>

                        <div class="forms-container">

                            <form action="sortName" method="get" class="sort-form2">
                                <select name="sortn" value="${sessionScope.acc.uid}" onchange="this.form.submit()">
                                    <option value="default">Sort by name</option>
                                    <option value="asc">A -> Z</option>
                                    <option value="desc">Z -> A</option>
                                </select>
                            </form>
                        </div>

                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Product ID</th>
                                    <th>Product Name</th>
                                    <th>Hidden Content</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="detail" items="${listOrder}">
                                    <tr>
                                        <td>${detail.productID}</td>
                                        <td>${detail.product.name}</td>
                                        <td>${detail.product.hiddenContent}</td>
                                        <td>${detail.date}</td>
                                        <td>
                                            ${detail.status}
                                            <br>
                                            <c:if test="${detail.status eq 'Wrong report'}">
                                                <a onclick="return confirm('Are you sure? - Require 50.000 VND')" class="btn btn-primary" href="updateReport?mua&status=buyer-request&id=${detail.productID}">Request admin</a>
                                            </c:if>
                                            <c:if test="${detail.status eq 'Seller request admin'}">
                                                <a onclick="return confirm('Are you sure? - Require 50.000 VND')" class="btn btn-primary" href="updateReport?mua&status=request&id=${detail.productID}">Accept</a>
                                            </c:if>
                                            <c:if test="${detail.status eq 'Processing'}">
                                                <a class="btn btn-primary" href="chat?id=${detail.product.sellerID * 1000 + detail.userID}">Contact</a>
                                            </c:if>
                                        </td>
                                        <td>
                                            <c:if test="${detail.status eq 'Pending'}">
                                                <a onclick="return confirm('Are you sure?')" class="btn btn-primary" href="update-order?status=Completed&id=${detail.id}">Accept</a>
                                                <a onclick="return confirm('Are you sure?')" class="btn btn-danger" href="update-order?status=Reported&id=${detail.id}">Report</a>
                                            </c:if>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                        <c:set var="page" value="${requestScope.page}"/>
                        <div class="pagination">
                            <c:forEach begin="1" end="${requestScope.num}" var="i">
                                <a class="${i==page?"active":""}" href="DonMua?page=${i}">${i}</a>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Bootstrap JS link (Optional if needed for Bootstrap features) -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>


        </body>
        <footer class="footer text-center">
            <div class="footer-content">
                <h3>Trang web Gi Cung Ban</h3>
                <a>Điện thoại: +84 0234 567 89</a>
                <a>Email: quangbz9512@gmail.com</a>
                <a>Địa chỉ: 123 Đường Lac Trung, Phường Hai Ba Trung, Thành phố Ha Noi, Việt Nam</a>
            </div>
        </footer>
    </html>
