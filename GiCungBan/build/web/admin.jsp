<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin</title>
        <link rel="icon" type="image/png" href="images/logo.png">
        <link href="css/admin-styles.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <header class="header">
            <a class="navbar-brand" href="adminHomeController"><i class="bi bi-briefcase"></i> Welcome, Admin!</a>
            <button class="sidebar-toggle"><i class="fas fa-bars"></i></button>
        </header>
        <aside class="sidebar">
            <nav class="nav">
                <ul>
                    <li><a href="adminHomeController">Account Customer</a></li>
                    <li><a href="reportAdminController">Report</a></li>
                    <li><a href="admininfo.jsp">View profile</a></li>
                </ul>
            </nav>
        </aside>

        <main class="main-content">

            <div class="container">
                <c:if test="${not empty errorMessage}">
                    <div class="alert alert-danger" role="alert" style="color: red; font-size: larger; font-weight: bold;">
                        ${errorMessage}
                    </div>
                </c:if>
                <%-- Hiển thị bảng Account --%>
                <c:if test="${not empty users}">
                    <h2>User List</h2>
                    <form action="adminHomeController" method="get" class="search-form">
                        <input type="text" name="searchUser" placeholder="Search by username">
                        <button type="submit" class="btn btn-primary">Search</button>
                    </form>

                    <form action="adminHomeController" method="get">
                        <label for="sortUser">Sort by Username:</label>
                        <select name="sortUser" id="sortUser" onchange="this.form.submit()">
                            <option value="default" selected>Default </option>
                            <option value="username_asc">Ascending</option>
                            <option value="username_desc">Descending</option>
                        </select>
                    </form>

                    <form action="adminHomeController" method="get">
                        <label for="sortEmail">Sort by Email:</label>
                        <select name="sortEmail" id="sortEmail" onchange="this.form.submit()">
                            <option value="default" selected>Default </option>
                            <option value="email_asc">Ascending</option>
                            <option value="email_desc">Descending</option>
                        </select>
                    </form>

                    <table class="user-table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Account Balance</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="user" items="${users}">
                                <tr>
                                    <td>${user.uid}</td>
                                    <td>${user.username}</td>
                                    <td>${user.email}</td>
                                    <td>${user.phonenumber}</td>
                                    <td>${user.amount}</td>
                                    <td><c:choose>
                                            <c:when test="${user.status eq 'Verified'}">

                                                <form action="adminAccount" method="post">
                                                    <input type="hidden" name="accountId" value="${user.uid}">
                                                    <input type="hidden" name="action" value="disable">
                                                    <button type="submit" class="disable-account-btn">Disabled</button>
                                                </form>
                                            </c:when>
                                            <c:when test="${user.status eq 'Disabled'}">

                                                <form action="adminAccount" method="post">
                                                    <input type="hidden" name="accountId" value="${user.uid}">
                                                    <input type="hidden" name="action" value="verify">
                                                    <button type="submit" class="verified-account-btn">Verified</button>
                                                </form>
                                            </c:when>
                                        </c:choose></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <div class="pagination">
                        <c:if test="${currentPage > 1}">
                            <a href="adminHomeController?page=${currentPage - 1}">Previous</a>
                        </c:if>

                        <c:forEach var="pageNum" begin="1" end="${numPages}">
                            <c:choose>
                                <c:when test="${pageNum eq currentPage}">
                                    <strong>${pageNum}</strong>
                                </c:when>
                                <c:otherwise>
                                    <a href="adminHomeController?page=${pageNum}">${pageNum}</a>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>

                        <c:if test="${currentPage < numPages}">
                            <a href="adminHomeController?page=${currentPage + 1}">Next</a>
                        </c:if>
                    </div>
                </c:if>

                <%-- Hiển thị bảng Report --%>
                <c:if test="${not empty reports}">
                    <div class="container">
                        <h2>Report List</h2>
                        <table class="report-table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>ID người báo cáo</th>
                                    <th>ID người bị báo cáo</th>
                                    <th>Ngày Report</th>
                                    <th>Reason</th>
                                    <th>Status</th>
                                    <th>Update Status</th>
                                    <th>Handle</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="report" items="${reports}">
                                    <tr>
                                        <td>${report.id}</td>
                                        <td>${report.reporterUserID}</td>
                                        <td>${report.reportedUserID}</td>
                                        <td>${report.reportDate}</td>
                                        <td>${report.reason}</td>
                                        <td>${report.status}</td>
                                        <td>
                                            <c:if test="${report.status != 'Completed'}">
                                                <form action="updateStatusReport" method="post">
                                                    <input type="hidden" name="reportId" value="${report.reporterUserID}">
                                                    <input type="hidden" name="reportId2" value="${report.reportedUserID}">
                                                    <input type="hidden" name="action" value="buyer">
                                                    <input type="hidden" name="id" value="${report.id}">
                                                    <button type="submit" class="report-correct-btn">Bên mua đúng</button>
                                                </form>
                                                <form action="updateStatusReport" method="post">
                                                    <input type="hidden" name="reportId" value="${report.reporterUserID}">
                                                    <input type="hidden" name="reportId2" value="${report.reportedUserID}">
                                                    <input type="hidden" name="action" value="seller">
                                                    <input type="hidden" name="id" value=${report.id}>
                                                    <button type="submit" class="report-correct-btn">Bên bán đúng</button>
                                                </form>
                                            </c:if>
                                        </td>
                                        <td>

                                            <c:if test="${report.status == 'Processing'}">
                                                <a class="btn btn-primary" href="chat?id=${report.reportedUserID * 1000 + report.reporterUserID}">Contact</a>
                                            </c:if>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </c:if>
            </div>
        </main>
    </body>
</html>
