<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Chi tiết sản phẩm</title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
        <link rel="icon" type="image/png" href="images/logo.png">
        <link href="css/Detail.css" rel="stylesheet" />
        <style>
            table{
                border-collapse: collapse
            }
            .content{
                margin: auto;
                width: 80%;
                float: left;
                margin-left: 30px;
                height: auto
            }
            .pagination {
                display: inline-block;
            }
            .pagination a {
                color: black;
                font-size: 22px;
                float: left;
                padding: 8px 16px;
                text-decoration: none;
            }
            .pagination a.active {
                background-color: #333;
                color: white;
            }
            .pagination a:hover:not(.active) {
                background-color: chocolate;
            </style>
        </head>
        <body>
            <header>
                <div class="header-left">

                </div>
                <div class="header-center">
                    <h1>GiCungBan</h1>

                </div>
                <div class="header-right">

                </div>
            </header>

            <c:if test="${sessionScope.acc != null}">
                <%-- Lấy sellerID từ sessionScope --%>
                <c:set var="sellerID" value="${sessionScope.acc.uid}" />
            </c:if>
            <nav>
                <a class="text-decoration-none" href="home">Publicly traded market</a>
                <a class="text-decoration-none" href="vnpay_pay.jsp">Payment management</a>
                <a class="text-decoration-none" href="manage">My sales order</a>
                <a class="text-decoration-none" href="DonMua">My purchase order</a>
                <c:if test="${sessionScope.acc !=null}" >
                    <a href="profile" class="text-decoration-none">Xin chào ${sessionScope.acc.username}</a>
                    <a class="text-decoration-none">Tài Khoản: <fmt:formatNumber type="number" pattern="#,##0" value="${amount}"/></a>
                    <button class="btn" onclick="openNotificationModal()">🔔</button>
                </c:if>
                <c:if test="${sessionScope.acc == null}" >
                    <a href="login.jsp" class="nav-item nav-link">Đăng nhập</a>                               
                </c:if>
            </nav>
            <main>
                <c:if test="${param.success ne null}">
                    <div class="alert alert-success" role="alert">
                        Success!
                    </div>
                </c:if>
                <c:if test="${param.nomoney ne null}">
                    <div class="alert alert-danger" role="alert">
                        Not enough money!
                    </div>
                </c:if>
                <c:if test="${param.sold ne null}">
                    <div class="alert alert-danger" role="alert">
                        Item sold!
                    </div>
                </c:if>

                <div class="product-detail-container">
                    <div class="product-info">
                        <p>${detail.imageLink}<p>
                        <h1>Tên sản phẩm: ${detail.name}</h1>
                        <h2>Giá sản phẩm: <fmt:formatNumber type="number" pattern="#,##0" value="${detail.price}"/></h2>
                        <p>Mô tả chi tiết: ${detail.description}</p>
                        <p>Tên người bán: ${detail.username}</p>
                        <p>Thông tin liên hệ: ${detail.contactMethod}</p>
                        <p>Đăng lần đầu: ${detail.createdAt}</p>
                        <p>Cập nhật lần cuối: ${detail.lastUpdated}</p>

                        <c:if test="${sessionScope.acc != null && detail.username != sessionScope.acc.username}">
                            <a href="buy?id=${detail.id}&price=${detail.price}" type="button" class="btn btn-primary" onclick="alert('Mua ngay')">Mua ngay</a>
                        </c:if>

                        <c:choose>
                            <c:when test="${sessionScope.acc != null && detail.username eq sessionScope.acc.username}">
                                <c:set var="isSeller" value="true" />
                            </c:when>
                            <c:otherwise>
                                <c:set var="isSeller" value="false" />
                            </c:otherwise>
                        </c:choose>

                        <a href="<c:if test='${isSeller}'>manage</c:if><c:if test='${!isSeller}'>viewProfile?username=${detail.username}</c:if>" class="btn btn-secondary profile-button">
                            Xem Profile Người Bán
                        </a>
                    </div>
                </div>
            </main>

           <footer class="footer">
                <div class="footer-content">
                    <h3>Trang web GiCungBan</h3>
                    <a>Điện thoại: +84 0234 567 89</a>
                    <a>Email: gicungban24@gmail.com</a>
                    <a>Địa chỉ: 123 Đường Lac Trung, Phường Hai Ba Trung, Thành phố Ha Noi, Việt Nam</a>
                </div>
            </footer>

            <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

        </body>
    </html>

