
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Profile</title>
        <link href="css/profile.css" rel="stylesheet" />
        <link rel="icon" type="image/png" href="images/logo.png">
    </head>
    <body>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css" integrity="sha256-2XFplPlrFClt0bIdPgpz8H7ojnk10H69xRqd9+uTShA=" crossorigin="anonymous" />
        <div class="container">
            <div class="row">
                <div class="col-12">

                    <div>
                        <h3>My Profile</h3>
                        <hr>
                    </div>
                    <div class="row mb-5 gx-5 justify-content-center align-items-center">

                        <div class="col-xxl-8 mb-5 mb-xxl-0">
                            <div class="bg-secondary-soft px-4 py-5 rounded">
                                <div class="row g-3 ">
                                    <h4 class="mb-4 mt-0">Contact detail</h4>


                                    <div class="col-md-6">
                                        <label class="form-label">Name</label>
                                        <input type="text" class="form-control" placeholder aria-label=" name" value="${a.username}" readonly>
                                    </div>



                                    <div class="col-md-6">
                                        <label class="form-label">Phone number</label>
                                        <input type="text" class="form-control" placeholder aria-label="Phone number" value="${a.phonenumber}" readonly>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label">Email</label>
                                        <input type="email" class="form-control" id="inputEmail4" value="${a.email}"" readonly>
                                    </div>

                                    <div class="col-md-6">
                                        <label class="form-label">Address</label>
                                        <input type="text" class="form-control" placeholder aria-label="Last name" value="${a.address}" readonly>
                                    </div>
                                </div> 
                            </div>

                            <div class="py-2">
                                <a href="update"><button type="submit"  class="btn btn-success btn-lg" value="">Update profile</button></a>
                                <a href="ChangePass.jsp"><button type="button"  class="btn btn-primary btn-lg">Change password</button></a>
                                <a href="logout"><button type="button" class="btn btn-danger btn-lg">Logout</button></a>
                            </div>  
                        </div> 

                    </div>

                </div>

            </div>

        </div>
        <div class="container-fluid">
            <div class="gap-3 d-md-flex justify-content-md-start">
                <a href="home"> <button type="button" class="btn btn-warning btn-lg my-custom-button">Return to Homepage</button></a>
            </div>
        </div>
        
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript">

        </script>
    </body>
</html>
