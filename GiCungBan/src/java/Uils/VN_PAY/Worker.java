
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Uils.VN_PAY;

import dal.DAO;
import dal.OrderDetailDAO;
import java.util.Calendar;
import java.util.List;
import model.Account;
import model.OrderDetail;
import model.Product;

/**
 *
 * @author lvhn1
 */
public class Worker {

    public static void updatePendingOrdersToCompleted() {
        OrderDetailDAO orderDetailDAO = new OrderDetailDAO();

        // Get current date
        Calendar calendar = Calendar.getInstance();
        java.sql.Date currentDate = new java.sql.Date(calendar.getTimeInMillis());

        // Get all pending order details
        List<OrderDetail> pendingOrders = orderDetailDAO.getAllOrderDetailsWithStatus("Pending");
        // Iterate through pending orders
        for (OrderDetail orderDetail : pendingOrders) {
            // Check if the order detail's date is more than 7 days from today
            if (isDateMoreThanSevenDaysAgo(orderDetail.getDate(), currentDate)) {
                // Update status to 'Completed'
                orderDetail.setStatus("Completed");
                orderDetailDAO.updateOrderDetail(orderDetail);
                
                // update seller balance
                Product product = orderDetail.getProduct();
                
                Account seller = new DAO().getById(product.getSellerID());
                seller.setAmount(seller.getAmount() + product.getPrice());
                new DAO().updateBalance(seller);
                
            }
        }
    }

    // Helper method to check if a date is more than 7 days ago
    private static boolean isDateMoreThanSevenDaysAgo(java.util.Date dateToCheck, java.util.Date currentDate) {
        long diffInMillies = currentDate.getTime() - dateToCheck.getTime();
        long diffInDays = diffInMillies / (1000 * 60 * 60 * 24);
        return diffInDays > 7;
    }

}
