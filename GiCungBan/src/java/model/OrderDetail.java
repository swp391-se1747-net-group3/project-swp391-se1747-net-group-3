/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.DAOproduct;
import java.util.Date;

/**
 *
 * @author lvhn1
 */
public class OrderDetail {
    
    private int id;
    private int orderID;
    private int productID;
    private String productName;
    private String hiddenContent;
    private double price;
    private Date date;
    private int userID;
    private String status;

    public OrderDetail() {
    }

    public OrderDetail(int id, int orderID, int productID, double price, Date date, int userID, String status) {
        this.id = id;
        this.orderID = orderID;
        this.productID = productID;
        this.price = price;
        this.date = date;
        this.userID = userID;
        this.status = status;
    }
    
    public OrderDetail(int productID, String productName, String hiddenContent, Date date, String status ) {
        this.productID = productID;
        this.productName = productName;
        this.hiddenContent = hiddenContent;
        this.date = date;
        this.status = status;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getHiddenContent() {
        return hiddenContent;
    }

    public void setHiddenContent(String hiddenContent) {
        this.hiddenContent = hiddenContent;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public Product getProduct() {
        return new DAOproduct().getProductById(productID);
    }
    
}
