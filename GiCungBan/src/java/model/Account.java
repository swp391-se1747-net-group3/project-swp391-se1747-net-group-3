/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author LENOVO
 */
public class Account {

    private int uid;
    private String username;
    private String email;
    private String password;
    private String address;
    private String phonenumber;
    private int role;
    private String code;
    private double amount;
    private String status;

    public Account() {
    }

    //Đăng ký tài khoản mới
    public Account(String username, String email, String password, String address, String phonenumber, int role, double amount, String status, String code) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.address = address;
        this.phonenumber = phonenumber;
        this.role = role;
        this.amount = amount;
        this.status = status;
        this.code = code; // Thêm trường mã xác minh
    }

    public Account(String username, String email, String password, String address, String phonenumber, int role, double amount, String status) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.address = address;
        this.phonenumber = phonenumber;
        this.role = role;
        this.amount = amount;
        this.status = status;
    }
    
    

    public Account(int uid, String username, String email, String password, String address, String phonenumber, int role, double amount) {
        this.uid = uid;
        this.username = username;
        this.email = email;
        this.password = password;
        this.address = address;
        this.phonenumber = phonenumber;
        this.role = role;
        this.amount = amount;
    }

    public Account(int uid, String username, String email, String password, String address, String phonenumber, int role) {
        this.uid = uid;
        this.username = username;
        this.email = email;
        this.password = password;
        this.address = address;
        this.phonenumber = phonenumber;
        this.role = role;
    }

    public Account(String username, String email, String address, String phonenumber) {
        this.username = username;
        this.email = email;
        this.address = address;
        this.phonenumber = phonenumber;
    }

    public Account(String email, String code) {
        this.email = email;
        this.code = code;
    }

    public Account(String username, String email, String password, String phonenumber, String code) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.phonenumber = phonenumber;
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Account{" + "uid=" + uid + ", username=" + username + ", email=" + email + ", password=" + password + ", address=" + address + ", phonenumber=" + phonenumber + ", role=" + role + ", code=" + code + ", amount=" + amount + ", status=" + status + '}';
    }

}
