/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author PC
 */
public class Demand {
    private int Id;    
    private int UserID;
    private String Name;
    private double Amount;

    public Demand() {
    }

    public Demand(int UserID, String Name, double Amount) {
        this.UserID = UserID;
        this.Name = Name;
        this.Amount = Amount;
    }

    public Demand(int Id, int UserID, String Name, double Amount) {
        this.Id = Id;
        this.UserID = UserID;
        this.Name = Name;
        this.Amount = Amount;
    }
    
    
        

    public Demand(String Name, double Amount) {
        this.Name = Name;
        this.Amount = Amount;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int UserID) {
        this.UserID = UserID;
    }
    
    

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double Amount) {
        this.Amount = Amount;
    }

    @Override
    public String toString() {
        return "Demand{" + "Id=" + Id + ", UserID=" + UserID + ", Name=" + Name + ", Amount=" + Amount + '}';
    }
    

      
    
}
