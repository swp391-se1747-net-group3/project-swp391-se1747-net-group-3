/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.OrderDetailDAO;
import java.sql.Timestamp;

/**
 *
 * @author ACER
 */
public class Product {

    private int id;
    private String name;
    private String description;
    private double price;
    private int sellerID;
    private String contactMethod;
    private String username;
    private String hiddenContent;
    private Timestamp createdAt;
    private Timestamp lastUpdated;
    private byte[] image;
    private String base64Image;
    private byte[] imageBytes;
  

    
    public Product() {
    }

    //Hien thi san pham trong home
    public Product(int id, String name, String description, double price, int sellerID, String contactMethod, String username, Timestamp createdAt, Timestamp lastUpdated) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.sellerID = sellerID;
        this.contactMethod = contactMethod;
        this.username = username;
        this.createdAt = createdAt;
        this.lastUpdated = lastUpdated;
    }

    // Them san pham
    public Product(String name, String description, double price, int sellerID, String contactMethod, String hiddenContent, Timestamp createdAt, Timestamp lastUpdated, byte[] image) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.sellerID = sellerID;
        this.contactMethod = contactMethod;
        this.hiddenContent = hiddenContent;
        this.createdAt = createdAt;
        this.lastUpdated = lastUpdated;
        this.image = image;
    }

    //Update san pham
    public Product(int id, String name, String description, double price, String contactMethod, String hiddenContent, Timestamp lastUpdated, byte[] image) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.contactMethod = contactMethod;
        this.hiddenContent = hiddenContent;
        this.lastUpdated = lastUpdated;
        this.image = image;
    }

    public Product(int id, String name, String description, double price, int sellerID, String contactMethod, String username, String hiddenContent, Timestamp createdAt, Timestamp lastUpdated) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.sellerID = sellerID;
        this.contactMethod = contactMethod;
        this.username = username;
        this.hiddenContent = hiddenContent;
        this.createdAt = createdAt;
        this.lastUpdated = lastUpdated;
    }

    
    // get Product by Id -- Details sản phẩm
    public Product(int id, String name, String description, double price, int sellerID, String contactMethod, String username, Timestamp createdAt, Timestamp lastUpdated, byte[] image) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.sellerID = sellerID;
        this.contactMethod = contactMethod;
        this.username = username;
        this.createdAt = createdAt;
        this.lastUpdated = lastUpdated;
        this.image = image;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getSellerID() {
        return sellerID;
    }

    public void setSellerID(int sellerID) {
        this.sellerID = sellerID;
    }

    public String getContactMethod() {
        return contactMethod;
    }

    public void setContactMethod(String contactMethod) {
        this.contactMethod = contactMethod;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Timestamp lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getHiddenContent() {
        return hiddenContent;
    }

    public void setHiddenContent(String hiddenContent) {
        this.hiddenContent = hiddenContent;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getBase64Image() {
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }

    public byte[] getImageBytes() {
        return imageBytes;
    }

    public void setImageBytes(byte[] imageBytes) {
        this.imageBytes = imageBytes;
       
    }
    
      public String getImageLink(){
        return "<img src=\"data:image/jpeg;base64," + this.base64Image + "\" style=\"max-height: 140px;\" />";
    }
      
      public OrderDetail getOrder() {
        return new OrderDetailDAO().getFirstOrderByProductID(id);
    }
    
    public String getStatusString() {
        OrderDetail order = new OrderDetailDAO().getFirstOrderByProductID(id);
        return order==null ? "Avaiable" : order.getStatus();
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", description=" + description + ", price=" + price + ", sellerID=" + sellerID + ", contactMethod=" + contactMethod + ", username=" + username + ", hiddenContent=" + hiddenContent + ", createdAt=" + createdAt + ", lastUpdated=" + lastUpdated + ", image=" + image + ", base64Image=" + base64Image + ", imageBytes=" + imageBytes + '}';
    }




}
