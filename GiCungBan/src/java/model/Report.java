/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Date;

/**
 *
 * @author lvhn1
 */
public class Report {

    private int id;
    private int reporterUserID;
    private int reportedUserID;
    private Date reportDate;
    private String reason;
    private String status;

    public Report() {
    }

    public Report(int id, int reporterUserID, int reportedUserID, Date reportDate, String reason, String status) {
        this.id = id;
        this.reporterUserID = reporterUserID;
        this.reportedUserID = reportedUserID;
        this.reportDate = reportDate;
        this.reason = reason;
        this.status = status;
    }

    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getReporterUserID() {
        return reporterUserID;
    }

    public void setReporterUserID(int reporterUserID) {
        this.reporterUserID = reporterUserID;
    }

    public int getReportedUserID() {
        return reportedUserID;
    }

    public void setReportedUserID(int reportedUserID) {
        this.reportedUserID = reportedUserID;
    }

    public Date getReportDate() {
        return reportDate;
    }

    public void setReportDate(Date reportDate) {
        this.reportDate = reportDate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    

}
