package dal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBContext {

    /*private static final String URL = "jdbc:mysql://127.0.0.1:3306/projectswp2";
    private static final String USER = "root";
    private static final String PASSWORD = "123456bm";*/
    
    private static final String URL = "jdbc:mysql://localhost:3306/projectswp2";
    private static final String USER = "root";
    private static final String PASSWORD = "Bopbin123";

    public static Connection getConnection() {
        Connection con = null;
        try {
            // Load the MySQL JDBC driver
            Class.forName("com.mysql.cj.jdbc.Driver");

            // Create the connection
            con = DriverManager.getConnection(URL, USER, PASSWORD);

            System.out.println("Connected to the database");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return con;
    }

    public static void closeConnection(Connection connection) {
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
                System.out.println("Connection closed");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void printConnectionStatus() {
        Connection connection = getConnection();
        if (connection != null) {
            System.out.println("Connection is successful.");
            closeConnection(connection);
        } else {
            System.out.println("Connection failed.");
        }
    }

    public static void main(String[] args) {
        try {
            DBContext.printConnectionStatus();

            // Continue with other database operations...
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
