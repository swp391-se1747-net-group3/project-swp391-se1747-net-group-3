package dal;

import static dal.DBContext.getConnection;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.OrderDetail;

/**
 * DAOOrderDetail class used for interacting with the database to manage order
 * details.
 */
public class OrderDetailDAO extends DBContext {

    public static void main(String[] args) {
        // Instantiate DAOOrderDetail
        OrderDetailDAO daoOrderDetail = new OrderDetailDAO();

        // Test insertOrderDetail method
        OrderDetail orderDetail = new OrderDetail(1, 2, 5, 10.99, Date.valueOf("2024-03-19"), 1, "Pending");
        boolean inserted = daoOrderDetail.insertOrderDetail(orderDetail);
        System.out.println("Order detail inserted: " + inserted);

        // Test getAllOrderDetails method
        List<OrderDetail> allOrderDetails = daoOrderDetail.getAllOrderDetails();
        System.out.println("All Order Details:");
        for (OrderDetail od : allOrderDetails) {
            System.out.println(od);
        }

        // Test getOrderDetailById method
        int orderDetailId = 1;
        OrderDetail foundOrderDetail = daoOrderDetail.getOrderDetailById(orderDetailId);
        System.out.println("Order Detail with ID " + orderDetailId + ": " + foundOrderDetail);

        // Test getOrderDetailsByUserId method
        int userId = 1;
        List<OrderDetail> orderDetailsByUserId = daoOrderDetail.getOrderDetailsByUserId(userId);
        System.out.println("Order Details for User ID " + userId + ":");
        for (OrderDetail od : orderDetailsByUserId) {
            System.out.println(od);
        }

        // Test updateOrderDetail method
        boolean updated = daoOrderDetail.updateOrderDetail(foundOrderDetail);
        System.out.println("Order detail updated: " + updated);
    }

    public OrderDetail getOrderDetailByProductID(int productID) {
        OrderDetail orderDetail = null;
        String sql = "SELECT * FROM orderdetails WHERE productID = ?";
        try ( Connection con = getConnection();  PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, productID);
            try ( ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    orderDetail = mapResultSetToOrderDetail(rs);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return orderDetail;
    }

    /**
     * Inserts a new order detail into the database.
     *
     * @param orderDetail The OrderDetail object to insert.
     * @return true if the insertion is successful, otherwise false.
     * @throws SQLException if a database access error occurs.
     */
    public boolean insertOrderDetail(OrderDetail orderDetail) {
        String sql = "INSERT INTO orderdetails (orderID, productID, price, date, userID, status) VALUES (?, ?, ?, ?, ?, ?)";
        try ( Connection con = getConnection();  PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, orderDetail.getOrderID());
            ps.setInt(2, orderDetail.getProductID());
            ps.setDouble(3, orderDetail.getPrice());
            ps.setDate(4, new Date(System.currentTimeMillis()));
            ps.setInt(5, orderDetail.getUserID());
            ps.setString(6, orderDetail.getStatus());

            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * Retrieves all order details from the database.
     *
     * @return A list of OrderDetail objects.
     * @throws SQLException if a database access error occurs.
     */
    public List<OrderDetail> getAllOrderDetails() {
        List<OrderDetail> orderDetails = new ArrayList<>();
        String sql = "SELECT * FROM orderdetails";
        try ( Connection con = getConnection();  PreparedStatement ps = con.prepareStatement(sql);  ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                OrderDetail orderDetail = mapResultSetToOrderDetail(rs);
                orderDetails.add(orderDetail);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return orderDetails;
    }

    /**
     * Retrieves an order detail from the database by its ID.
     *
     * @param id The ID of the order detail to retrieve.
     * @return The OrderDetail object if found, otherwise null.
     * @throws SQLException if a database access error occurs.
     */
    public OrderDetail getOrderDetailById(int id) {
        OrderDetail orderDetail = null;
        String sql = "SELECT * FROM orderdetails WHERE id = ?";
        try ( Connection con = getConnection();  PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, id);
            try ( ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    orderDetail = mapResultSetToOrderDetail(rs);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return orderDetail;
    }

    /**
     * Retrieves all order details for a given user ID from the database.
     *
     * @param userID The ID of the user.
     * @return A list of OrderDetail objects for the given user ID.
     * @throws SQLException if a database access error occurs.
     */
    public List<OrderDetail> getOrderDetailsByUserId(int userID) {
        List<OrderDetail> orderDetails = new ArrayList<>();
        String sql = "SELECT * FROM orderdetails WHERE userID = ?";
        try ( Connection con = getConnection();  PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, userID);
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    OrderDetail orderDetail = mapResultSetToOrderDetail(rs);
                    orderDetails.add(orderDetail);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return orderDetails;
    }

    /**
     * Updates an existing order detail in the database.
     *
     * @param orderDetail The OrderDetail object with updated information.
     * @return true if the update is successful, otherwise false.
     * @throws SQLException if a database access error occurs.
     */
    public boolean updateOrderDetail(OrderDetail orderDetail) {
        String sql = "UPDATE orderdetails SET orderID = ?, productID = ?, price = ?, date = ?, userID = ?, status = ? WHERE id = ?";
        try ( Connection con = getConnection();  PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, orderDetail.getOrderID());
            ps.setInt(2, orderDetail.getProductID());
            ps.setDouble(3, orderDetail.getPrice());
            ps.setDate(4, (Date) orderDetail.getDate());
            ps.setInt(5, orderDetail.getUserID());
            ps.setString(6, orderDetail.getStatus());
            ps.setInt(7, orderDetail.getId());

            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    // Helper method to map ResultSet to OrderDetail object
    private OrderDetail mapResultSetToOrderDetail(ResultSet rs) throws SQLException {
        OrderDetail orderDetail = new OrderDetail();
        orderDetail.setId(rs.getInt("id"));
        orderDetail.setOrderID(rs.getInt("orderID"));
        orderDetail.setProductID(rs.getInt("productID"));
        orderDetail.setPrice(rs.getDouble("price"));
        orderDetail.setDate(rs.getDate("date"));
        orderDetail.setUserID(rs.getInt("userID"));
        orderDetail.setStatus(rs.getString("status"));
        return orderDetail;
    }

    public List<OrderDetail> getAllOrderDetailsWithStatus(String status) {
        List<OrderDetail> orderDetails = new ArrayList<>();
        String sql = "SELECT * FROM orderdetails WHERE status = ?";
        try ( Connection con = getConnection();  PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, status);
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    OrderDetail orderDetail = mapResultSetToOrderDetail(rs);
                    orderDetails.add(orderDetail);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return orderDetails;
    }
    
    public OrderDetail getFirstOrderByProductID(int productID) {
        OrderDetail orderDetail = null;
        String sql = "SELECT * FROM orderdetails WHERE productID = ? LIMIT 1";
        try ( Connection con = getConnection();  PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, productID);
            try ( ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    orderDetail = mapResultSetToOrderDetail(rs);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return orderDetail;
    }
    
    public List<OrderDetail> searchByNameOrderid(int orderID, String search) {
        List<OrderDetail> list = new ArrayList<>();
        String sql = "SELECT * FROM orderdetails WHERE userID = ? AND productID IN (SELECT id FROM product WHERE `name` LIKE ?)"; // Thêm điều kiện cho sellerID

        try ( Connection con = this.getConnection();  PreparedStatement stm = con.prepareStatement(sql)) {
            stm.setInt(1, orderID);
            stm.setString(2, "%" + search + "%"); // Set giá trị cho sellerID
            try ( ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    OrderDetail order = new OrderDetail(rs.getInt(1),
                            rs.getInt(2),
                            rs.getInt(3),
                            rs.getDouble(4),
                            rs.getDate(5),
                            rs.getInt(6),
                            rs.getString(7));

                    list.add(order);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
}
