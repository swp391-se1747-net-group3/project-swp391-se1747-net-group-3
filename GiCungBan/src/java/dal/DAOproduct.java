/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import model.Demand;
import model.Product;

/**
 * DAOproduct class used for interacting with the database to manage products.
 */
public class DAOproduct extends DBContext {

    /**
     * Retrieves all products from the database.
     *
     * @return A list of Product objects.
     * @throws SQLException if a database access error occurs.
     */
    public static boolean insertNewProduct(Product product) {
        try ( Connection con = DBContext.getConnection();  PreparedStatement ps = con.prepareStatement("INSERT INTO product (name, description, price, sellerID, contactMethod, hiddenContent, createdAt, lastUpdated, image) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)")) {
            ps.setString(1, product.getName());
            ps.setString(2, product.getDescription());
            ps.setDouble(3, product.getPrice());
            ps.setInt(4, product.getSellerID());
            ps.setString(5, product.getContactMethod());
            ps.setString(6, product.getHiddenContent());
            ps.setTimestamp(7, new Timestamp(System.currentTimeMillis())); // Thời gian tạo là thời điểm hiện tại
            ps.setTimestamp(8, new Timestamp(System.currentTimeMillis())); // Thời gian cập nhật là thời điểm hiện tại
            ps.setBytes(9, product.getImage()); // Thêm dữ liệu hình ảnh

            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean insertNewDemand(Demand demand) {
        try ( Connection con = DBContext.getConnection();  PreparedStatement ps = con.prepareStatement("INSERT INTO Demand (userID, name, amount) VALUES (?, ?, ?)")) {
            ps.setInt(1, demand.getUserID());
            ps.setString(2, demand.getName());
            ps.setDouble(3, demand.getAmount());

            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean updateProduct(Product product) {
        try ( Connection con = DBContext.getConnection();  PreparedStatement ps = con.prepareStatement("UPDATE product SET name = ?, description = ?, price = ?, contactMethod = ?, hiddenContent = ? , lastUpdated = ? , image = ? WHERE id = ?")) {
            ps.setString(1, product.getName());
            ps.setString(2, product.getDescription());
            ps.setDouble(3, product.getPrice());
            ps.setString(4, product.getContactMethod());
            ps.setString(5, product.getHiddenContent());
            ps.setTimestamp(6, new Timestamp(System.currentTimeMillis())); // Thời gian cập nhật là thời điểm hiện tại
            ps.setBytes(7, product.getImage());
            ps.setInt(8, product.getId());

            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean updateAmount(int userID) {
        String sql = "UPDATE user AS u "
                + "INNER JOIN Demand AS d ON u.id = d.userID "
                + "SET u.amount = u.amount + d.amount "
                + "WHERE d.userID = ?";

        try ( Connection connection = DBContext.getConnection();  PreparedStatement statement = connection.prepareStatement(sql)) {

            // Truyền giá trị userID vào câu lệnh SQL
            statement.setInt(1, userID);

            // Thực thi câu lệnh UPDATE
            int rowsAffected = statement.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

    }

    public static boolean deleteProduct(int id) {
        try ( Connection con = DBContext.getConnection();  PreparedStatement ps = con.prepareStatement("DELETE FROM product WHERE id = ?")) {
            ps.setInt(1, id);

            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean deleteDemand(int Id) {
        try ( Connection con = DBContext.getConnection();  PreparedStatement ps = con.prepareStatement("DELETE FROM Demand WHERE id = ?")) {
            ps.setInt(1, Id);

            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<Product> getAllProductsBySellerID(int sellerID) {
        List<Product> list = new ArrayList<>();
        try ( Connection con = getConnection();  PreparedStatement ps = con.prepareStatement("SELECT product.id, product.name, product.description, product.sellerID, product.price, product.sellerID, product.contactMethod, product.hiddenContent, user.username, product.createdAt, product.lastUpdated , product.image "
                + "FROM product "
                + "INNER JOIN user ON product.sellerID = user.id WHERE product.sellerID = ?")) {
            ps.setInt(1, sellerID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Product product = new Product();
                product.setId(rs.getInt("id"));
                product.setName(rs.getString("name"));
                product.setDescription(rs.getString("description"));
                product.setPrice(rs.getDouble("price"));
                product.setUsername(rs.getString("username"));
                product.setContactMethod(rs.getString("contactMethod"));
                product.setHiddenContent(rs.getString("hiddenContent"));
                product.setSellerID(rs.getInt("sellerID"));
                // Lấy ngày tạo và ngày cập nhật từ kết quả truy vấn
                product.setCreatedAt(rs.getTimestamp("createdAt"));
                product.setLastUpdated(rs.getTimestamp("lastUpdated"));
                product.setImage(rs.getBytes("image")); // Sử dụng image, không phải imageData

                list.add(product);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public List<Product> searchByNameSellerid(String search, int sellerID) {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT * "
                + "FROM product "
                + "WHERE Name LIKE ? AND sellerId = ?"; // Thêm điều kiện cho sellerID

        try ( Connection con = this.getConnection();  PreparedStatement stm = con.prepareStatement(sql)) {
            stm.setString(1, "%" + search + "%");
            stm.setInt(2, sellerID); // Set giá trị cho sellerID
            try ( ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    Product product = new Product(rs.getInt("id"),
                            rs.getString("name"),
                            rs.getString("description"),
                            rs.getDouble("price"),
                            rs.getInt("sellerID"),
                            rs.getString("contactMethod"),
                            rs.getString("hiddenContent"),
                            rs.getTimestamp("createdAt"),
                            rs.getTimestamp("lastUpdated"));
                    product.setImage(rs.getBytes("image"));

                    list.add(product);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public double getAmountForUser(int userId) {
        double amount = 0.0; // Giá trị mặc định là 0

        // Chuỗi truy vấn SQL để lấy giá trị amount cho user
        String query = "SELECT amount FROM user WHERE id = ?";

        try (
                // Mở kết nối đến cơ sở dữ liệu
                 Connection connection = DBContext.getConnection(); // Chuẩn bị câu lệnh truy vấn
                  PreparedStatement preparedStatement = connection.prepareStatement(query);) {
            // Thiết lập tham số cho câu lệnh truy vấn
            preparedStatement.setInt(1, userId);

            // Thực thi câu lệnh truy vấn
            try ( ResultSet resultSet = preparedStatement.executeQuery()) {
                // Kiểm tra xem có kết quả trả về không
                if (resultSet.next()) {
                    // Lấy giá trị amount từ kết quả truy vấn
                    amount = resultSet.getDouble("amount");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // Xử lý ngoại lệ hoặc trả về giá trị mặc định
        }

        return amount;
    }
    
    public Product getProductById(int id) {
        Product product = null;
        String sql = "SELECT * FROM product WHERE id = ?";
        try ( Connection con = getConnection();  PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                product = new Product();
                product.setId(rs.getInt("id"));
                product.setName(rs.getString("name"));
                product.setDescription(rs.getString("description"));
                product.setPrice(rs.getDouble("price"));
                product.setSellerID(rs.getInt("sellerID"));
                product.setContactMethod(rs.getString("contactMethod"));
                product.setHiddenContent(rs.getString("hiddenContent"));
                product.setCreatedAt(rs.getTimestamp("createdAt"));
                product.setLastUpdated(rs.getTimestamp("lastUpdated"));
                product.setImage(rs.getBytes("image"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return product;
    }

    public static void main(String[] args) {
        // Khởi tạo DAOproduct
        DAOproduct dao = new DAOproduct();

        // Giả sử bạn có một từ khoá tìm kiếm và một sellerId
        String keyword = "a"; // Từ khoá tìm kiếm
        int sellerId = 3; // Id của người bán

        // Gọi phương thức searchByNameSellerid để tìm kiếm sản phẩm
        List<Product> resultList = dao.searchByNameSellerid(keyword, sellerId);

        // In ra các sản phẩm được tìm thấy
        if (resultList.isEmpty()) {
            System.out.println("Không tìm thấy sản phẩm nào.");
        } else {
            System.out.println("Các sản phẩm được tìm thấy:");
            for (Product product : resultList) {
                System.out.println(product);
            }
        }
    }

}
