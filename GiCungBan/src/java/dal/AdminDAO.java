/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Account;
import model.Report;

/**
 *
 * @author ACER
 */
public class AdminDAO {

    public List<Account> getUserByIdNotEqualsSix() {
        List<Account> accounts = new ArrayList<>();
        String sql = "SELECT * FROM user WHERE id <> 6";

        try ( Connection con = DBContext.getConnection();  PreparedStatement ps = con.prepareStatement(sql);  ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                Account account = new Account();
                account.setUid(rs.getInt("id"));
                account.setUsername(rs.getString("username"));
                account.setEmail(rs.getString("email"));
                account.setPassword(rs.getString("password"));
                account.setAddress(rs.getString("address"));
                account.setPhonenumber(rs.getString("phoneNum"));
                account.setRole(rs.getInt("role"));
                account.setAmount(rs.getDouble("amount"));
                account.setStatus(rs.getString("status"));

                accounts.add(account);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return accounts;
    }

    public boolean disableAccount(int accountId) {
        try ( Connection con = DBContext.getConnection();  PreparedStatement ps = con.prepareStatement("UPDATE user SET status = ? WHERE id = ?")) {
            ps.setString(1, "Disabled");
            ps.setInt(2, accountId);
            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public boolean verifyAccount(int accountId) {
        try ( Connection con = DBContext.getConnection();  PreparedStatement ps = con.prepareStatement("UPDATE user SET status = ? WHERE id = ?")) {
            ps.setString(1, "Verified");
            ps.setInt(2, accountId);
            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public List<Report> getAllReports() {
        List<Report> reports = new ArrayList<>();
        String query = "SELECT * FROM reports";

        try ( Connection connection = DBContext.getConnection();  PreparedStatement statement = connection.prepareStatement(query);  ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                int reporterUserID = resultSet.getInt("reporterUserID");
                int reportedUserID = resultSet.getInt("reportedUserID");
                Date reportDate = resultSet.getDate("reportDate");
                String reason = resultSet.getString("reason");
                String status = resultSet.getString("status");

                // Tạo đối tượng Report từ dữ liệu lấy từ cơ sở dữ liệu
                Report report = new Report(id, reporterUserID, reportedUserID, reportDate, reason, status);
                // Thêm báo cáo vào danh sách
                reports.add(report);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return reports;
    }

    //Search bằng ID
    public Report getReportById(int id) {
        Report report = null;
        try ( Connection connection = DBContext.getConnection();  PreparedStatement statement = connection.prepareStatement("SELECT * FROM reports WHERE id = ?")) {
            statement.setInt(1, id);
            try ( ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    report = new Report();
                    report.setId(resultSet.getInt("id"));
                    report.setReporterUserID(resultSet.getInt("reporterUserID"));
                    report.setReportedUserID(resultSet.getInt("reportedUserID"));
                    report.setReportDate(resultSet.getDate("reportDate"));
                    report.setReason(resultSet.getString("reason"));
                    report.setStatus(resultSet.getString("status"));
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return report;
    }

    public Account getUserByEmailNotEqualsSix(String email) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Account user = null;

        try {
            con = DBContext.getConnection();
            String sql = "SELECT * FROM user WHERE email LIKE ? AND id <> 6";
            ps = con.prepareStatement(sql);
            ps.setString(1, "%" + email + "%");
            rs = ps.executeQuery();

            if (rs.next()) {
                user = new Account();
                user.setUid(rs.getInt("id"));
                user.setUsername(rs.getString("username"));
                user.setEmail(rs.getString("email"));
                user.setPhonenumber(rs.getString("phoneNum"));
                user.setAmount(rs.getDouble("amount"));
                user.setStatus(rs.getString("status"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

        return user;
    }

    public void updateReportStatusToProcessed(int id) {
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            conn = DBContext.getConnection();
            String sql = "UPDATE reports SET status = 'Completed' WHERE id = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void addMoneyToBuyer(int reportId, double price) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DBContext.getConnection();
            // Cập nhật tài khoản của bên mua bằng cách cộng thêm 50000 vào số dư hiện tại
            String query = "UPDATE user SET amount = amount + ? WHERE id = (SELECT reporterUserID FROM reports WHERE reporterUserID = ?)";
            stmt = conn.prepareStatement(query);
            stmt.setDouble(1, price +  50000); // Số tiền cần cộng vào tài khoản
            stmt.setInt(2, reportId);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void addMoneyToSeller(int reportId, double price) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DBContext.getConnection();
            // Cập nhật tài khoản của bên bán bằng cách cộng thêm 50000 vào số dư hiện tại
            String query = "UPDATE user SET amount = amount + ? WHERE id = (SELECT reportedUserID FROM reports WHERE reportedUserID = ?)";
            stmt = conn.prepareStatement(query);
            stmt.setDouble(1, price + 50000); // Số tiền cần cộng vào tài khoản
            stmt.setInt(2, reportId);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void updateOrderStatusToCompleted(int id) {
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            conn = DBContext.getConnection();
            String sql = "UPDATE orderdetails SET status = 'Completed' WHERE productID = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public double getPriceByReportID(int reportId) {
        int productID = 0;
        double price = 0.0;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            // Thực hiện kết nối tới cơ sở dữ liệu (database)
            connection = DBContext.getConnection();

            // Chuẩn bị câu lệnh SQL
            String sql = "SELECT o.price "
                    + "FROM orderdetails o "
                    + "INNER JOIN product p ON o.productID = p.id "
                    + "INNER JOIN user u ON p.sellerID = u.id "
                    + "INNER JOIN reports r ON u.id = r.reportedUserID "
                    + "WHERE r.reportedUserID = ?";

            // Tạo đối tượng PreparedStatement
            preparedStatement = connection.prepareStatement(sql);

            // Thiết lập tham số cho câu lệnh SQL
            preparedStatement.setInt(1, reportId);

            // Thực thi truy vấn
            resultSet = preparedStatement.executeQuery();

            // Kiểm tra nếu có kết quả trả về
            if (resultSet.next()) {
                // Lấy giá từ kết quả truy vấn
                price = resultSet.getDouble("price");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // Đóng ResultSet, PreparedStatement và Connection để giải phóng tài nguyên
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        // Trả về giá
        return price;
    }
    
    public int getProductIDByReportID(int reportId) {
    int productID = 0;
    Connection connection = null;
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;

    try {
        // Thực hiện kết nối tới cơ sở dữ liệu (database)
        connection = DBContext.getConnection();

        // Chuẩn bị câu lệnh SQL
        String sql = "SELECT o.productID "
                + "FROM orderdetails o "
                + "INNER JOIN product p ON o.productID = p.id "
                + "INNER JOIN user u ON p.sellerID = u.id "
                + "INNER JOIN reports r ON u.id = r.reportedUserID "
                + "WHERE r.reportedUserID = ?";

        // Tạo đối tượng PreparedStatement
        preparedStatement = connection.prepareStatement(sql);

        // Thiết lập tham số cho câu lệnh SQL
        preparedStatement.setInt(1, reportId);

        // Thực thi truy vấn
        resultSet = preparedStatement.executeQuery();

        // Kiểm tra nếu có kết quả trả về
        if (resultSet.next()) {
            // Lấy productID từ kết quả truy vấn
            productID = resultSet.getInt("productID");
        }
    } catch (SQLException e) {
        e.printStackTrace();
    } finally {
        // Đóng ResultSet, PreparedStatement và Connection để giải phóng tài nguyên
        try {
            if (resultSet != null) {
                resultSet.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Trả về productID
    return productID;
}

    
    public static void main(String[] args) {
        AdminDAO adminDAO = new AdminDAO();
        
        // Giả sử bạn có một reportId cần truyền vào để kiểm tra
        int reportId = 1;
        
        // Gọi phương thức để kiểm tra
        double price = adminDAO.getPriceByReportID(reportId);
        
        // In ra giá đã lấy được
        System.out.println("Giá đã lấy được từ report ID " + reportId + ": " + price);
    }

}
