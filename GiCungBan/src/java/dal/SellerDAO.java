package dal;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Account;
import model.Product;
import java.net.URLDecoder;

public class SellerDAO {

    public Account getAccountByUsername(String username) {
        Account account = null;
        try ( Connection con = DBContext.getConnection();  PreparedStatement ps = con.prepareStatement("SELECT * FROM user WHERE username = ?")) {
            String decodedUsername = URLDecoder.decode(username, "UTF-8");
            ps.setString(1, username);
            try ( ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    account = new Account();
                    account.setUid(rs.getInt("id"));
                    account.setUsername(rs.getString("username"));
                    account.setEmail(rs.getString("email"));
                    account.setPassword(rs.getString("password"));
                    account.setAddress(rs.getString("address"));
                    account.setPhonenumber(rs.getString("phoneNum"));
                    account.setRole(rs.getInt("role"));
                }
            }
        } catch (SQLException | UnsupportedEncodingException ex) {
        ex.printStackTrace();
        }
        return account;
    }

    public List<Product> getProductsByAccountId(int accountId) {
        List<Product> productList2 = new ArrayList<>();
        try ( Connection con = DBContext.getConnection();  PreparedStatement ps = con.prepareStatement("SELECT * FROM product WHERE sellerID = ?")) {
            ps.setInt(1, accountId);
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Product product = new Product();
                    product.setId(rs.getInt("id"));
                    product.setName(rs.getString("name"));
                    product.setDescription(rs.getString("description"));
                    product.setPrice(rs.getDouble("price"));
                    product.setContactMethod(rs.getString("contactmethod"));
                    product.setCreatedAt(rs.getTimestamp("createdAt"));
                    product.setLastUpdated(rs.getTimestamp("lastUpdated"));
                    // Thêm các trường thông tin khác của sản phẩm nếu cần thiết
                    productList2.add(product);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return productList2;
    }
    
    
  


    public static void main(String[] args) {
        SellerDAO sellerDAO = new SellerDAO();
        int accountId = 3; // Thay đổi accountId tùy theo dữ liệu trong cơ sở dữ liệu
        List<Product> productList = sellerDAO.getProductsByAccountId(accountId);
        if (!productList.isEmpty()) {
            System.out.println("\nProducts for account ID " + accountId + ":");
            for (Product product : productList) {
                System.out.println("Product ID: " + product.getId());
                System.out.println("Product Name: " + product.getName());
                // In ra các thông tin khác của sản phẩm
            }
        } else {
            System.out.println("No products found for account ID: " + accountId);
        }
    }
}
