/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Account;

/**
 *
 * @author ACER
 */
public class SortAdminDAO {
    public List<Account> SortUserDESCNotEqualsSix() {
        List<Account> accounts = new ArrayList<>();
        String sql = "SELECT * FROM user WHERE id <> 6 ORDER BY username DESC;";

        try ( Connection con = DBContext.getConnection();  PreparedStatement ps = con.prepareStatement(sql);  ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                Account account = new Account();
                account.setUid(rs.getInt("id"));
                account.setUsername(rs.getString("username"));
                account.setEmail(rs.getString("email"));
                account.setPassword(rs.getString("password"));
                account.setAddress(rs.getString("address"));
                account.setPhonenumber(rs.getString("phoneNum"));
                account.setRole(rs.getInt("role"));
                account.setAmount(rs.getDouble("amount"));
                account.setStatus(rs.getString("status"));

                accounts.add(account);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return accounts;
    }
    
    public List<Account> SortUserASCNotEqualsSix() {
        List<Account> accounts = new ArrayList<>();
        String sql = "SELECT * FROM user WHERE id <> 6 ORDER BY username ASC;";

        try ( Connection con = DBContext.getConnection();  PreparedStatement ps = con.prepareStatement(sql);  ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                Account account = new Account();
                account.setUid(rs.getInt("id"));
                account.setUsername(rs.getString("username"));
                account.setEmail(rs.getString("email"));
                account.setPassword(rs.getString("password"));
                account.setAddress(rs.getString("address"));
                account.setPhonenumber(rs.getString("phoneNum"));
                account.setRole(rs.getInt("role"));
                account.setAmount(rs.getDouble("amount"));
                account.setStatus(rs.getString("status"));

                accounts.add(account);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return accounts;
    }
    
    public List<Account> SortEmailDESCNotEqualsSix() {
        List<Account> accounts = new ArrayList<>();
        String sql = "SELECT * FROM user WHERE id <> 6 ORDER BY email DESC;";

        try ( Connection con = DBContext.getConnection();  PreparedStatement ps = con.prepareStatement(sql);  ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                Account account = new Account();
                account.setUid(rs.getInt("id"));
                account.setUsername(rs.getString("username"));
                account.setEmail(rs.getString("email"));
                account.setPassword(rs.getString("password"));
                account.setAddress(rs.getString("address"));
                account.setPhonenumber(rs.getString("phoneNum"));
                account.setRole(rs.getInt("role"));
                account.setAmount(rs.getDouble("amount"));
                account.setStatus(rs.getString("status"));

                accounts.add(account);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return accounts;
    }
    
    public List<Account> SortEmailASCNotEqualsSix() {
        List<Account> accounts = new ArrayList<>();
        String sql = "SELECT * FROM user WHERE id <> 6 ORDER BY email ASC;";

        try ( Connection con = DBContext.getConnection();  PreparedStatement ps = con.prepareStatement(sql);  ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                Account account = new Account();
                account.setUid(rs.getInt("id"));
                account.setUsername(rs.getString("username"));
                account.setEmail(rs.getString("email"));
                account.setPassword(rs.getString("password"));
                account.setAddress(rs.getString("address"));
                account.setPhonenumber(rs.getString("phoneNum"));
                account.setRole(rs.getInt("role"));
                account.setAmount(rs.getDouble("amount"));
                account.setStatus(rs.getString("status"));

                accounts.add(account);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return accounts;
    }
}
