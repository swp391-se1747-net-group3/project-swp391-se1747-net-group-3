/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Chat;

public class ChatDAO {
    private Connection connection;

    public ChatDAO() {
        this.connection = DBContext.getConnection();
    }

    // Insert a new chat message into the database
    public boolean insertChat(Chat chat) {
        String query = "INSERT INTO chat (chatId, message) VALUES (?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, chat.getChatId());
            statement.setString(2, chat.getMessage());
            int rowsInserted = statement.executeUpdate();
            return rowsInserted > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    // Retrieve all chat messages from the database
    public List<Chat> getAllChats() {
        List<Chat> chats = new ArrayList<>();
        String query = "SELECT * FROM chat";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                int chatId = resultSet.getInt("chatId");
                String message = resultSet.getString("message");
                Chat chat = new Chat();
                chat.setId(id);
                chat.setChatId(chatId);
                chat.setMessage(message);
                chats.add(chat);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return chats;
    }

    // Retrieve a chat message by its ID from the database
    public Chat getChatById(int chatId) {
        String query = "SELECT * FROM chat WHERE id = ?";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, chatId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                int id = resultSet.getInt("id");
                String message = resultSet.getString("message");
                Chat chat = new Chat();
                chat.setId(id);
                chat.setChatId(chatId);
                chat.setMessage(message);
                return chat;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    // Update a chat message in the database
    public boolean updateChat(Chat chat) {
        String query = "UPDATE chat SET message = ? WHERE id = ?";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, chat.getMessage());
            statement.setInt(2, chat.getId());
            int rowsUpdated = statement.executeUpdate();
            return rowsUpdated > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    // Delete a chat message by its ID from the database
    public boolean deleteChat(int chatId) {
        String query = "DELETE FROM chat WHERE id = ?";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, chatId);
            int rowsDeleted = statement.executeUpdate();
            return rowsDeleted > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public List<Chat> getAllChatsByChatId(int chatId) {
        List<Chat> chats = new ArrayList<>();
        String query = "SELECT * FROM chat WHERE chatId = ?";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, chatId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String message = resultSet.getString("message");
                Chat chat = new Chat();
                chat.setId(id);
                chat.setChatId(chatId);
                chat.setMessage(message);
                chats.add(chat);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return chats;
    }
    
}

