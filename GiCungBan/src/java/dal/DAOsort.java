/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.OrderDetail;
import model.Product;

/**
 *
 * @author LENOVO
 */
public class DAOsort extends DBContext {

    public List<OrderDetail> getOrderDetailsSortedByName(int userID, String sortOrder) {
        List<OrderDetail> orderDetails = new ArrayList<>();
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = this.getConnection();// Code để lấy kết nối đến cơ sở dữ liệu

            // Câu lệnh SQL để lấy danh sách sản phẩm sắp xếp theo giá tiền và sellerID
            String sql = "SELECT p.id, p.name, p.hiddenContent, od.date, od.status FROM orderdetails od JOIN product p ON od.productID = p.id where userID = ? ORDER BY p.name " + sortOrder;

            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, userID);
            rs = stmt.executeQuery();

            // Lặp qua kết quả và thêm các sản phẩm vào danh sách
            while (rs.next()) {
                // Code để tạo đối tượng Product từ kết quả của câu lệnh SQL và thêm vào danh sách
                OrderDetail orderDetail = new OrderDetail();
                orderDetail.setProductID(rs.getInt(1));
                orderDetail.setProductName(rs.getString(2));
                orderDetail.setHiddenContent(rs.getString(3));
                orderDetail.setDate(rs.getDate(4));
                orderDetail.setStatus(rs.getString(5));
                // Thêm vào danh sách
                orderDetails.add(orderDetail);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // Xử lý ngoại lệ nếu cần
        } finally {
            // Đóng các đối tượng ResultSet, PreparedStatement và Connection
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return orderDetails;
    }

    public List<OrderDetail> getListOrderByPage(List<OrderDetail> list, int start, int end) {
        ArrayList<OrderDetail> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public List<Product> sortProductsPriceBySellerID(int sellerID, String sortOrder) {
        List<Product> productList = new ArrayList<>();
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = this.getConnection();// Code để lấy kết nối đến cơ sở dữ liệu

            // Câu lệnh SQL để lấy danh sách sản phẩm sắp xếp theo giá tiền và sellerID
            String sql = "SELECT product.id, product.name, product.description, product.price, product.sellerID, product.contactMethod, product.hiddenContent, user.username, product.createdAt, product.lastUpdated , product.image FROM product INNER JOIN user ON product.sellerID = user.id WHERE product.sellerID = ? ORDER BY product.price " + sortOrder;

            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, sellerID);
            rs = stmt.executeQuery();

            // Lặp qua kết quả và thêm các sản phẩm vào danh sách
            while (rs.next()) {
                // Code để tạo đối tượng Product từ kết quả của câu lệnh SQL và thêm vào danh sách
                Product product = new Product();
                product.setId(rs.getInt("id"));
                product.setName(rs.getString("name"));
                product.setDescription(rs.getString("description"));
                product.setPrice(rs.getDouble("price"));
                product.setSellerID(rs.getInt("sellerID"));
                product.setContactMethod(rs.getString("contactMethod"));
                product.setHiddenContent(rs.getString("hiddenContent"));
                product.setCreatedAt(rs.getTimestamp("createdAt"));
                product.setLastUpdated(rs.getTimestamp("lastUpdated"));
                product.setImage(rs.getBytes("image"));
                // Thêm vào danh sách
                productList.add(product);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // Xử lý ngoại lệ nếu cần
        } finally {
            // Đóng các đối tượng ResultSet, PreparedStatement và Connection
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return productList;
    }

    public static void main(String[] args) {
        DAO dao = new DAO();
        DAOsort ds = new DAOsort();
        // Thử nghiệm sắp xếp tên sản phẩm từ A đến Z
        List<Product> productsNameAsc = ds.sortProductsPriceBySellerID(1, "asc");
        System.out.println("Sản phẩm sắp xếp theo tên từ A đến Z:");
        for (Product p : productsNameAsc) {
            System.out.println(p.getName() + " - " + p.getDescription() + " - " + p.getContactMethod());
        }

        // Thử nghiệm sắp xếp tên sản phẩm từ Z đến A
        // List<Product> productsNameDesc = dao.getAllProductSortedByName("giam"); // Sử dụng "giam" theo đoạn mã của bạn
        // System.out.println("\nSản phẩm sắp xếp theo tên từ Z đến A:");
        // for (Product p : productsNameDesc) {
        //    System.out.println(p.getName() + " - " + p.getPrice());
        // }
    }
}
