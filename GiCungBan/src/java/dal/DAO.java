/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Product;

/**
 *
 * @author LENOVO
 */
public class DAO extends DBContext {

    private Connection con = null;
    private PreparedStatement stm = null;
    ResultSet rs = null;

    public void registerUser(String username, String email, String password, String address, String phoneNumber) throws SQLException {
        try {
            Connection connection = DBContext.getConnection();
            if (connection == null) {
                throw new SQLException("Failed to connect to the database.");
            }

            // SQL query for account registration
            String query = "INSERT INTO user (username, email, password, address, phoneNum, role, amount, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

            stm = connection.prepareStatement(query);
            stm.setString(1, username);
            stm.setString(2, email);
            stm.setString(3, password);
            stm.setString(4, address);
            stm.setString(5, phoneNumber);
            stm.setInt(6, 1); // Mặc định role là 1
            stm.setDouble(7, 0); // Mặc định amount là 0
            stm.setString(8, "Unverified"); // Mặc định status là unverified

            stm.executeUpdate();
        } finally {
            // Đóng connection và statement
            if (stm != null) {
                stm.close();
            }
            if (con != null) {
                con.close();
            }
        }
    }

    public boolean checkEmailExists(String email) throws SQLException {
        boolean exists = false;
        try {
            con = DBContext.getConnection();
            String sql = "SELECT * FROM user WHERE email = ?";
            stm = con.prepareStatement(sql);
            stm.setString(1, email);
            rs = stm.executeQuery();
            if (rs.next()) {

                int count = rs.getInt(1);

                if (count > 0) {
                    exists = true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // Close the resources
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stm != null) {
                    stm.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return exists;
    }

    public void updateAccountStatus(String email) throws SQLException {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DBContext.getConnection();
            String query = "UPDATE user SET status = 'Verified' WHERE email = ?";
            statement = connection.prepareStatement(query);
            statement.setString(1, email);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("Error updating account status.", e);
        } finally {
            DBContext.closeConnection(connection);
            if (statement != null) {
                statement.close();
            }
        }
    }

    public Account getById(int id) {
        try {
            Connection con = DBContext.getConnection();
            String sql = "SELECT * FROM user WHERE id = ?";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setInt(1, id);

            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                Account a = new Account(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7));
                a.setAmount(rs.getDouble("amount"));
                return a;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // Close the connection
            DBContext.closeConnection(con);
        }
        return null;
    }

    public Account login(String email, String password) throws SQLException {
        // Database connection
        Connection connection = DBContext.getConnection();
        if (connection == null) {
            throw new SQLException("Failed to connect to the database.");
        }

        // SQL query for checking login credentials
        String query = "SELECT * FROM user where `email` = ? and `password` = ?;";

        try {
            // Create a prepared statement
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);

            // Execute the query
            ResultSet rs = preparedStatement.executeQuery();

            // Check if any records were found
            if (rs.next()) {
                Account a = new Account(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7));
                a.setAmount(rs.getDouble("amount"));
                return a;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("Error during login process.", e);
        } finally {
            // Close the connection
            DBContext.closeConnection(connection);
        }

        // Return null if login credentials are invalid
        return null;
    }

    public Account changePass(String pass, String email) {
        try {
            Connection con = DBContext.getConnection();
            String sql = "UPDATE user SET `password`=? WHERE `email`=?;";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setString(1, pass);
            stm.setString(2, email);
            stm.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

// Cập nhật Account
    public void update(Account a) {
        try {
            Connection con = DBContext.getConnection();
            String sql = "UPDATE user SET `username`=?, `phoneNum`=?, `address`=? WHERE `email`=?;";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setString(1, a.getUsername());
            stm.setString(2, a.getPhonenumber());
            stm.setString(3, a.getAddress());
            stm.setString(4, a.getEmail());
            stm.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    // Cập nhật tiền trong tài khoản
    public void updateBalance(Account a) {
        try {
            Connection con = DBContext.getConnection();
            String sql = "UPDATE user SET `amount`=? WHERE `id`=?;";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setDouble(1, a.getAmount());
            stm.setInt(2, a.getUid());
            stm.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public List<Product> getAllProduct() {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT product.id, product.name, product.description, product.price, product.sellerID, product.contactMethod, user.username, product.createdAt, product.lastUpdated "
                + "FROM product "
                + "INNER JOIN user ON product.sellerID = user.id";
        try ( Connection con = this.getConnection();  PreparedStatement stm = con.prepareStatement(sql);  ResultSet rs = stm.executeQuery()) {

            while (rs.next()) {
                Product product = new Product(rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("description"),
                        rs.getDouble("price"),
                        rs.getInt("sellerID"),
                        rs.getString("contactMethod"),
                        rs.getString("username"),
                        rs.getTimestamp("createdAt"),
                        rs.getTimestamp("lastUpdated"));

                list.add(product);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Product> searchByName(String search) {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT product.id, product.name, product.description, product.price, product.sellerID, product.contactMethod, user.username, product.createdAt, product.lastUpdated "
                + "FROM product "
                + "INNER JOIN user ON product.sellerID = user.id "
                + "WHERE Name LIKE ?";

        try ( Connection con = this.getConnection();  PreparedStatement stm = con.prepareStatement(sql)) {
            stm.setString(1, "%" + search + "%");
            try ( ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    Product product = new Product(rs.getInt("id"),
                            rs.getString("name"),
                            rs.getString("description"),
                            rs.getDouble("price"),
                            rs.getInt("sellerID"),
                            rs.getString("contactMethod"),
                            rs.getString("username"),
                            rs.getTimestamp("createdAt"),
                            rs.getTimestamp("lastUpdated"));

                    list.add(product);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public Product getProductByID(String id) {
        String sql = "SELECT product.id, product.name, product.description, product.price, product.sellerID, product.contactMethod, user.username, product.createdAt, product.lastUpdated, product.image  "
                + "FROM product "
                + "INNER JOIN user ON product.sellerID = user.id "
                + "WHERE product.id = ?";

        try ( Connection con = this.getConnection();  PreparedStatement stm = con.prepareStatement(sql)) {
            stm.setString(1, id);
            try ( ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    return new Product(rs.getInt("id"),
                            rs.getString("name"),
                            rs.getString("description"),
                            rs.getDouble("price"),
                            rs.getInt("sellerID"),
                            rs.getString("contactMethod"),
                            rs.getString("username"),
                            rs.getTimestamp("createdAt"),
                            rs.getTimestamp("lastUpdated"),
                            rs.getBytes("image"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Product> getListByPage(List<Product> list, int start, int end) {
        ArrayList<Product> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }
    
    public List<Account> getListByPageAccount(List<Account> list, int start, int end) {
        ArrayList<Account> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }
    
    public List<Product> getAllProductSortedByPrice(String sort) {
        List<Product> list = new ArrayList<>();
        String sqlBase = "SELECT product.id, product.name, product.description, product.price, product.sellerID, product.contactMethod, user.username, product.createdAt, product.lastUpdated "
                + "FROM product "
                + "INNER JOIN user ON product.sellerID = user.id";
        String orderByClause;
        if ("desc".equalsIgnoreCase(sort)) {
            orderByClause = " ORDER BY product.price DESC";
        } else {
            orderByClause = " ORDER BY product.price ASC";
        }
        String sql = sqlBase + orderByClause;

        try ( Connection con = this.getConnection();  PreparedStatement stm = con.prepareStatement(sql);  ResultSet rs = stm.executeQuery()) {
            while (rs.next()) {
                Product product = new Product(rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("description"),
                        rs.getDouble("price"),
                        rs.getInt("sellerID"),
                        rs.getString("contactMethod"),
                        rs.getString("username"),
                        rs.getTimestamp("createdAt"),
                        rs.getTimestamp("lastUpdated"));
                list.add(product);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;

    }

    public List<Product> getAllProductSortedByName(String sortn) {
        List<Product> list = new ArrayList<>();
        String sqlBase = "SELECT product.id, product.name, product.description, product.price, product.sellerID, product.contactMethod, user.username, product.createdAt, product.lastUpdated "
                + "FROM product "
                + "INNER JOIN user ON product.sellerID = user.id";
        String orderByClause;
        if ("giam".equalsIgnoreCase(sortn)) {
            orderByClause = " ORDER BY product.name DESC";
        } else {
            orderByClause = " ORDER BY product.name ASC";
        }
        String sql = sqlBase + orderByClause;

        try ( Connection con = this.getConnection();  PreparedStatement stm = con.prepareStatement(sql);  ResultSet rs = stm.executeQuery()) {
            while (rs.next()) {
                Product product = new Product(rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("description"),
                        rs.getDouble("price"),
                        rs.getInt("sellerID"),
                        rs.getString("contactMethod"),
                        rs.getString("username"),
                        rs.getTimestamp("createdAt"),
                        rs.getTimestamp("lastUpdated"));
                list.add(product);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static void main(String[] args) {
        // Khởi tạo một đối tượng DAO
        DAO dao = new DAO();

        // Giả sử email của tài khoản cần được cập nhật trạng thái là "verified"
        String email = "example@example.com";

        try {
            // Gọi phương thức để cập nhật trạng thái của tài khoản
            dao.updateAccountStatus(email);
            System.out.println("Account status updated successfully for email: " + email);
        } catch (SQLException e) {
            System.err.println("Error updating account status: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
