/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Report;

public class ReportDAO {
    private Connection connection;

    public ReportDAO() {
        this.connection = DBContext.getConnection();
    }

    // Method to insert a new report into the database
    public boolean insertReport(Report report) {
        String query = "INSERT INTO reports (reporterUserID, reportedUserID, reportDate, reason, status) VALUES (?, ?, ?, ?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, report.getReporterUserID());
            statement.setInt(2, report.getReportedUserID());
            statement.setDate(3, new java.sql.Date(report.getReportDate().getTime()));
            statement.setString(4, report.getReason());
            statement.setString(5, report.getStatus());
            int rowsInserted = statement.executeUpdate();
            return rowsInserted > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    // Method to retrieve all reports from the database
    public List<Report> getAllReports() {
        List<Report> reports = new ArrayList<>();
        String query = "SELECT * FROM reports";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                int reporterUserID = resultSet.getInt("reporterUserID");
                int reportedUserID = resultSet.getInt("reportedUserID");
                Date reportDate = resultSet.getDate("reportDate");
                String reason = resultSet.getString("reason");
                String status = resultSet.getString("status");
                Report report = new Report();
                report.setId(id);
                report.setReporterUserID(reporterUserID);
                report.setReportedUserID(reportedUserID);
                report.setReportDate(reportDate);
                report.setReason(reason);
                report.setStatus(status);
                reports.add(report);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return reports;
    }

    // Method to retrieve a report by its ID from the database
    public Report getReportById(int reportId) {
        String query = "SELECT * FROM reports WHERE id = ?";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, reportId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                int reporterUserID = resultSet.getInt("reporterUserID");
                int reportedUserID = resultSet.getInt("reportedUserID");
                Date reportDate = resultSet.getDate("reportDate");
                String reason = resultSet.getString("reason");
                String status = resultSet.getString("status");
                Report report = new Report();
                report.setId(reportId);
                report.setReporterUserID(reporterUserID);
                report.setReportedUserID(reportedUserID);
                report.setReportDate(reportDate);
                report.setReason(reason);
                report.setStatus(status);
                return report;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    // Method to update all fields of a report in the database
    public boolean updateReport(Report report) {
        String query = "UPDATE reports SET reporterUserID = ?, reportedUserID = ?, reportDate = ?, reason = ?, status = ? WHERE id = ?";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, report.getReporterUserID());
            statement.setInt(2, report.getReportedUserID());
            statement.setDate(3, new java.sql.Date(report.getReportDate().getTime()));
            statement.setString(4, report.getReason());
            statement.setString(5, report.getStatus());
            statement.setInt(6, report.getId());
            int rowsUpdated = statement.executeUpdate();
            return rowsUpdated > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    // Method to update the status of a report in the database
    public boolean updateReportStatus(int reportId, String newStatus) {
        String query = "UPDATE reports SET status = ? WHERE id = ?";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, newStatus);
            statement.setInt(2, reportId);
            int rowsUpdated = statement.executeUpdate();
            return rowsUpdated > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}


