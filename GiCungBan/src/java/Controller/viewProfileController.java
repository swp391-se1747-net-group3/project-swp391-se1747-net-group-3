/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import dal.DAO;
import dal.DAOproduct;
import dal.SellerDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Account;
import model.Product;

/**
 *
 * @author ACER
 */
public class viewProfileController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet viewProfileController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet viewProfileController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        DAOproduct daow = new DAOproduct();
        DAO daoa = new DAO();
        SellerDAO dao = new SellerDAO();
        Account seller = dao.getAccountByUsername(username);
        Account user = (Account) request.getSession().getAttribute("acc");
        if (user != null) {
            int userId = user.getUid();
            double amount = daow.getAmountForUser(userId);
            Account a = daoa.getById(userId);
            request.setAttribute("a", a);
            request.setAttribute("amount", amount);
// Đặt các thuộc tính vào request để chuyển đến viewProfile.jsp
            if (seller != null) {
                int sellerId = seller.getUid();
                List<Product> productList = dao.getProductsByAccountId(sellerId);

                // Đặt các thuộc tính vào request để chuyển đến viewProfile.jsp
                request.setAttribute("seller", seller);
                request.setAttribute("productList", productList);

                // Chuyển hướng request và response tới viewProfile.jsp
                request.getRequestDispatcher("viewProfile.jsp").forward(request, response);
            } else {

            }
        } else {
            response.sendRedirect("login.jsp");
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
