/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import dal.DAO;
import dal.DAOproduct;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Product;
import java.sql.Timestamp;
import java.util.Base64;

/**
 *
 * @author PC
 */
public class ManageProductController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ManageProductController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ManageProductController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // processRequest(request, response);
        Account user = (Account) request.getSession().getAttribute("acc");

        if (user != null) {
            // Nếu có người dùng đăng nhập
            int sellerID = user.getUid();
            DAOproduct dao = new DAOproduct();

            // Lấy danh sách sản phẩm của người bán dựa trên sellerID
            List<Product> productList = dao.getAllProductsBySellerID(sellerID);

            for (Product product : productList) {
                byte[] imageData = product.getImage();
                // Chuyển đổi dữ liệu hình ảnh thành Base64
                if (imageData != null) {
                    String base64Image = Base64.getEncoder().encodeToString(product.getImage());
                    product.setBase64Image(base64Image);
                }

            }

            // Số sản phẩm trên mỗi trang
            int numPerPage = 6;
            int size = productList.size();
            int numPages = (size % numPerPage == 0 ? (size / numPerPage) : ((size / numPerPage)) + 1);

            // Xác định trang hiện tại
            int page = 1;
            String pageParam = request.getParameter("page");
            if (pageParam != null && !pageParam.isEmpty()) {
                page = Integer.parseInt(pageParam);
                if (page < 1 || page > numPages) {
                    // Xử lý nếu số trang không hợp lệ
                    response.sendRedirect("error.jsp"); // Chuyển hướng đến trang lỗi
                    return;
                }
            }

            // Tính vị trí bắt đầu và kết thúc của danh sách sản phẩm trên trang hiện tại
            int start = (page - 1) * numPerPage;
            int end = Math.min(page * numPerPage, size);

            // Lấy danh sách sản phẩm cho trang hiện tại
            List<Product> productsOnPage = productList.subList(start, end);
            int userId = user.getUid();
            double amount = dao.getAmountForUser(userId);
            request.setAttribute("amount", amount);
            // Đặt các thuộc tính cho request để truyền dữ liệu sang trang JSP
            request.setAttribute("page", page);
            request.setAttribute("data", productsOnPage);
            request.setAttribute("listC", productList);
            request.setAttribute("num", numPages);

            // Chuyển hướng đến trang DonBan.jsp
            request.getRequestDispatcher("DonBan.jsp").forward(request, response);
        } else {
            // Nếu không có người dùng đăng nhập, chuyển hướng đến trang đăng nhập
            response.sendRedirect("login.jsp");
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
