/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.SQLException;
import model.Account;

/**
 *
 * @author LENOVO
 */
public class RegisterServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String pass = request.getParameter("password");
        String address = request.getParameter("address");
        String phone = request.getParameter("phone");
        String repass = request.getParameter("repassword");

        HttpSession session = request.getSession();
        if (!pass.equals(repass)) {
            session.setAttribute("pass", pass);
            session.setAttribute("repass", repass);
            session.setMaxInactiveInterval(5);
            request.setAttribute("repassError", "Passwords do not match");
            request.getRequestDispatcher("registration.jsp").forward(request, response);
            return;
        }

        // Tạo một tài khoản mới
        Account account = new Account();
        account.setUsername(name);
        account.setEmail(email);
        account.setPhonenumber(phone);
        account.setAddress(address);
        account.setPassword(pass);

        DAO dao = new DAO();
        SendEmail sendEmail = new SendEmail();

        try {
            // Kiểm tra xem email đã tồn tại chưa
            if (dao.checkEmailExists(email)) {
                request.setAttribute("already", "Email already exists");
                request.getRequestDispatcher("registration.jsp").forward(request, response);
            } else {
                // Thêm tài khoản mới vào cơ sở dữ liệu
                dao.registerUser(account.getUsername(), account.getEmail(), account.getPassword(), account.getAddress(), account.getPhonenumber());


                // Gửi email xác thực
                String code = sendEmail.getRandom();
                account.setCode(code);

                boolean emailSent = sendEmail.sendEmail(account);
                if (emailSent) {
                    // Chuyển hướng đến trang thông báo xác thực
                    session.setAttribute("email", email);
                    session.setAttribute("authcode", code);
                    response.sendRedirect("verification.jsp");
                } else {
                    // Xử lý lỗi khi gửi email
                    request.setAttribute("errorMessage", "Failed to send verification email. Please try again later.");
                    request.getRequestDispatcher("registration.jsp").forward(request, response);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // Xử lý lỗi khi truy vấn cơ sở dữ liệu
            request.setAttribute("errorMessage", "Database error. Please try again later.");
            request.getRequestDispatcher("registration.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
