/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import dal.AdminDAO;
import dal.DAO;
import dal.DAOproduct;
import dal.SortAdminDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import model.Account;
import model.Demand;
import model.Product;

/**
 *
 * @author PC
 */
public class AdminHomeController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String searchEmail = request.getParameter("searchUser");
        String sortUser = request.getParameter("sortUser");
        String sortEmail = request.getParameter("sortEmail");
        AdminDAO dao = new AdminDAO();
        SortAdminDAO sort = new SortAdminDAO();
        DAO paging = new DAO();
        List<Account> users;
        int page, numPerPage = 6;

        if (searchEmail != null && !searchEmail.isEmpty()) {
            // Thực hiện tìm kiếm người dùng bằng ID
            Account searchedUser = dao.getUserByEmailNotEqualsSix(searchEmail);
            if (searchedUser != null) {
                // Nếu tìm thấy người dùng, tạo một danh sách chứa người dùng này
                users = new ArrayList<>();
                users.add(searchedUser);
            } else {
                // Nếu không tìm thấy người dùng, gửi thông báo cho người dùng
                request.setAttribute("errorMessage", "User with email " + searchEmail + " not found.");
                // Đưa người dùng trở lại trang admin.jsp
                request.getRequestDispatcher("admin.jsp").forward(request, response);
                return;
            }
        } else {
            if (sortUser != null && !sortUser.isEmpty()) {
                if (sortUser.equals("username_asc")) {
                    users = sort.SortUserASCNotEqualsSix();
                } else if (sortUser.equals("username_desc")) {
                    users = sort.SortUserDESCNotEqualsSix();
                } else {
                    users = dao.getUserByIdNotEqualsSix();
                }

            } else if (sortEmail != null && !sortEmail.isEmpty()) {
                if (sortEmail.equals("email_asc")) {
                    users = sort.SortEmailASCNotEqualsSix();
                } else if (sortEmail.equals("email_desc")) {
                    users = sort.SortEmailDESCNotEqualsSix();
                } else {
                    users = dao.getUserByIdNotEqualsSix();
                }
            } else {
                // Nếu không có tham số sắp xếp, lấy tất cả người dùng (ngoại trừ id=6)
                users = dao.getUserByIdNotEqualsSix();
            }

        }
        int size = users.size();
        int num = (size % numPerPage == 0 ? (size / numPerPage) : ((size / numPerPage)) + 1);
        String xpage = request.getParameter("page");
        if (xpage == null) {
            page = 1;
        } else {
            page = Integer.parseInt(xpage);
        }
        int start, end;
        start = (page - 1) * numPerPage;
        end = Math.min(page * numPerPage, size);
        users = paging.getListByPageAccount(users, start, end);
        request.setAttribute("users", users);
        request.setAttribute("numPages", num);
        request.setAttribute("currentPage", page);
        request.getRequestDispatcher("admin.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
