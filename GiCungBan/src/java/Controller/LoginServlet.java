/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import dal.DAO;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Random;
import javax.imageio.ImageIO;
import model.Account;

/**
 *
 * @author LENOVO
 */
public class LoginServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LoginServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LoginServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
          response.setContentType("image/jpeg");

        int width = 150;
        int height = 50;
        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics graphics = bufferedImage.getGraphics();
        graphics.setColor(Color.BLACK);
        graphics.fillRect(0, 0, width, height);
        graphics.setColor(Color.WHITE);
        graphics.setFont(new Font("Arial", Font.BOLD, 24));
        String captchaText = generateCaptchaText();
        HttpSession session = request.getSession(true);
        session.setAttribute("captcha", captchaText);
        graphics.drawString(captchaText, 40, 40);
        ImageIO.write(bufferedImage, "jpeg", response.getOutputStream());
    }

    // Phương thức để tạo chuỗi CAPTCHA ngẫu nhiên
    private String generateCaptchaText() {
        int length = 6;
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuilder captchaText = new StringBuilder();
        for (int i = 0; i < length; i++) {
            captchaText.append(chars.charAt(random.nextInt(chars.length())));
        }
        return captchaText.toString();
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
       String email = request.getParameter("email");
        String pass = request.getParameter("password");
        String userEnteredCaptcha = request.getParameter("captchaInput");
        HttpSession session = request.getSession();
        String actualCaptcha = (String) session.getAttribute("captcha");
        
        String targetEmail = "lfsmarch18@gmail.com";
        String targetPassword = "12345";
        
        try {
            DAO dao = new DAO();
            Account a = dao.login(email, pass);
            if (a == null) {
                request.setAttribute("mess", "Wrong username or password");
                request.getRequestDispatcher("login.jsp").forward(request, response);
            } else {
                if (userEnteredCaptcha != null && userEnteredCaptcha.equals(actualCaptcha)) {
                     if (targetEmail.equals(email) && targetPassword.equals(pass)) {
                        session.setMaxInactiveInterval(5000);
                        response.sendRedirect("adminHomeController");
                    } else {
                        session.setAttribute("acc", a);
                        session.setMaxInactiveInterval(5000);
                        response.sendRedirect("home");
                    }
                } else {
                    request.setAttribute("mess2", "Wrong captcha!");
                    request.getRequestDispatcher("login.jsp").forward(request, response);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
