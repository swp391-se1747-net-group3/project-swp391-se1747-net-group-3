/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.SQLException;
import model.Account;

/**
 *
 * @author ACER
 */
public class VerifyCode extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            if (session != null) {
                String email = (String) session.getAttribute("email");
                String authCode = (String) session.getAttribute("authcode");
                String code = request.getParameter("authcode");
                if (code != null && authCode != null) {
                    try {
                        if (code.equals(authCode)) {
                            DAO dao = new DAO();
                            dao.updateAccountStatus(email);
                            request.setAttribute("registered", "Đăng ký tài khoản thành công");
                            request.getRequestDispatcher("login.jsp").forward(request, response);
                            return;
                        } else {
                            session.setAttribute("error", "Mã xác minh không đúng. Vui lòng thử lại.");
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                        request.setAttribute("error", "Lỗi cập nhật trạng thái tài khoản. Vui lòng thử lại sau.");
                    }
                } else {
                    request.setAttribute("error", "Lỗi xác minh. Vui lòng thử lại.");
                }
                request.getRequestDispatcher("verification.jsp").forward(request, response);
            } else {
                // Xử lý trường hợp session null
                response.sendRedirect("login.jsp"); // Hoặc chuyển hướng tới trang lỗi
            }
        }

    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
