/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package Controller;

import dal.DAO;
import dal.DAOproduct;
import dal.OrderDetailDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Date;
import model.Account;
import model.OrderDetail;
import model.Product;

/**
 *
 * @author lvhn1
 */
@WebServlet(name="BuyController", urlPatterns={"/buy"})
public class BuyController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet BuyController</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet BuyController at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        
        int id = Integer.parseInt(request.getParameter("id"));
        double price = Double.parseDouble(request.getParameter("price"));
        Account account = (Account) request.getSession().getAttribute("acc");
        
        Product product = new DAOproduct().getProductById(id);
        
        OrderDetail orderCheck = new OrderDetailDAO().getOrderDetailByProductID(id);
        
        if (account.getAmount() < price) {
            response.sendRedirect("detail?nomoney&pid=" + id);
            return;
        }
        
        if (orderCheck != null && orderCheck.getStatus().toLowerCase().equals("success")) {
            response.sendRedirect("detail?sold&pid=" + id);
            return;
        }
        
        account.setAmount(account.getAmount() - price);
        new DAO().updateBalance(account);
        
        OrderDetail order = new OrderDetail();
        order.setUserID(account.getUid());
        order.setProductID(id);
        order.setPrice(price);
        order.setStatus("Pending");
        order.setDate(new Date());
        new OrderDetailDAO().insertOrderDetail(order);
        
        response.sendRedirect("detail?success&pid=" + id);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
