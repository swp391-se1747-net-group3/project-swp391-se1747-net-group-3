/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package Controller;

import dal.DAO;
import dal.DAOproduct;
import dal.OrderDetailDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Account;
import model.OrderDetail;

/**
 *
 * @author lvhn1
 */
@WebServlet(name="MyOrderController", urlPatterns={"/DonMua"})
public class MyOrderController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MyOrderController</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MyOrderController at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        
        Account user = (Account) request.getSession().getAttribute("acc");
        DAO dao = new DAO();
        DAOproduct daow = new DAOproduct();
        if (user != null) {
        //request.setAttribute("listOrder", new OrderDetailDAO().getOrderDetailsByUserId(account.getUid()));
        List<OrderDetail> orderDetails = new OrderDetailDAO().getOrderDetailsByUserId(user.getUid());
        
    // Số sản phẩm trên mỗi trang
            int numPerPage = 6;
            int size = orderDetails.size();
            int numPages = (size % numPerPage == 0 ? (size / numPerPage) : ((size / numPerPage)) + 1);

            // Xác định trang hiện tại
            int page = 1;
            String pageParam = request.getParameter("page");
            if (pageParam != null && !pageParam.isEmpty()) {
                page = Integer.parseInt(pageParam);
                if (page < 1 || page > numPages) {
                    // Xử lý nếu số trang không hợp lệ
                    response.sendRedirect("error.jsp"); // Chuyển hướng đến trang lỗi
                    return;
                }
            }

            // Tính vị trí bắt đầu và kết thúc của danh sách sản phẩm trên trang hiện tại
            int start = (page - 1) * numPerPage;
            int end = Math.min(page * numPerPage, size);

            // Lấy danh sách sản phẩm cho trang hiện tại
            List<OrderDetail> productsOnPage = orderDetails.subList(start, end);
            
            int userId = user.getUid();
            double amount = daow.getAmountForUser(userId);
            Account a = dao.getById(userId);
            request.setAttribute("a", a);
            request.setAttribute("amount", amount);
    // Lưu danh sách đơn hàng chi tiết của trang hiện tại và thông tin phân trang vào request attribute
            request.setAttribute("page", page);
            request.setAttribute("data", productsOnPage);
            request.setAttribute("listOrder", orderDetails);
            request.setAttribute("num", numPages);
        request.getRequestDispatcher("DonMua.jsp").forward(request, response);
        } else {
            response.sendRedirect("login.jsp");
        }
        
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
