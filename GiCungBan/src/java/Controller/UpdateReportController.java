/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import dal.DAO;
import dal.DAOproduct;
import dal.NotificationDAO;
import dal.OrderDetailDAO;
import dal.ReportDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Date;
import model.Account;
import model.Notification;
import model.OrderDetail;
import model.Product;
import model.Report;

/**
 *
 * @author lvhn1
 */
@WebServlet(name = "UpdateReportController", urlPatterns = {"/updateReport"})
public class UpdateReportController extends HttpServlet {

    private static int REQUEST_FEE = 50000;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int productId = Integer.parseInt(request.getParameter("id"));
        String status = request.getParameter("status");

        Product product = new DAOproduct().getProductById(productId);
        OrderDetail order = product.getOrder();
        Notification notification = new Notification();

        String resStatus = "";

        switch (status) {
            case "correct":
                resStatus = "Correct report";
                new OrderDetailDAO().updateOrderDetail(order);

                Account account = new DAO().getById(order.getUserID());
                account.setAmount(account.getAmount() + product.getPrice());
                new DAO().updateBalance(account);

                notification.setUserId(order.getUserID());
                notification.setContent("Người bán xác nhận khiếu nại của bạn - sản phẩm: " + product.getName());
                new NotificationDAO().insertNotification(notification);
                break;

            case "wrong":
                resStatus = "Wrong report";

                notification.setUserId(order.getUserID());
                notification.setContent("Người bán phủ nhận khiếu nại của bạn - sản phẩm: " + product.getName());
                new NotificationDAO().insertNotification(notification);
                break;

            case "seller-request":
                resStatus = "Seller request admin";
                notification.setUserId(order.getUserID());
                notification.setContent("Người bán đã yêu cầu admin can thiệp khiếu nại của bạn - sản phẩm: " + product.getName());
                new NotificationDAO().insertNotification(notification);
                break;

            case "buyer-request":
                resStatus = "Buyer request admin";
                notification.setUserId(product.getSellerID());
                notification.setContent("Người mua đã yêu cầu admin can thiệp khiếu nại của họ - sản phẩm: " + product.getName());
                new NotificationDAO().insertNotification(notification);
                break;
                
            case "request":
                resStatus = "Processing";

                Report report = new Report();
                report.setReporterUserID(order.getUserID());
                report.setReportedUserID(product.getSellerID());
                report.setStatus("Processing");
                report.setReportDate(new Date());
                report.setReason("Wrong hidden content");
                new ReportDAO().insertReport(report);
                
                notification.setUserId(order.getUserID());
                notification.setContent("Yêu cầu admin can thiệp khiếu nại đã gửi - sản phẩm: " + product.getName());
                new NotificationDAO().insertNotification(notification);
                
                notification.setUserId(product.getSellerID());
                notification.setContent("Yêu cầu admin can thiệp khiếu nại đã gửi - sản phẩm: " + product.getName());
                new NotificationDAO().insertNotification(notification);

                Account buyer = new DAO().getById(order.getUserID());
                buyer.setAmount(buyer.getAmount() - REQUEST_FEE);
                new DAO().updateBalance(buyer);

                Account seller = new DAO().getById(product.getSellerID());
                seller.setAmount(seller.getAmount() - REQUEST_FEE);
                new DAO().updateBalance(seller);
        }

        order.setStatus(resStatus);
        new OrderDetailDAO().updateOrderDetail(order);

        if (request.getParameter("mua") != null) {
            response.sendRedirect("DonMua?success");
        } else {
            response.sendRedirect("manage?success");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
