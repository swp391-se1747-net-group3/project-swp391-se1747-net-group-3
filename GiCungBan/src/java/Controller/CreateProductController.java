/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import dal.DAO;
import dal.DAOproduct;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Product;

@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 2, // 2 MB
        maxFileSize = 1024 * 1024 * 10, // 10 MB
        maxRequestSize = 1024 * 1024 * 50 // 50 MB
)
/**
 *
 * @author PC
 */
public class CreateProductController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        double price = Double.parseDouble(request.getParameter("price"));
        String contactMethod = request.getParameter("contactMethod");
        String hiddenContent = request.getParameter("hiddenContent");
        // Assuming you have the seller's user ID available in the session or elsewhere
        int sellerID = ((Account) request.getSession().getAttribute("acc")).getUid();
        
        Account account = new DAO().getById(sellerID);
        if (account.getAmount() < 5000) {
            response.sendRedirect("error.jsp");
            return;
        }
        
        account.setAmount(account.getAmount() - 5000);
        new DAO().updateBalance(account);

        Part imagePart = request.getPart("image");
        InputStream imageStream = imagePart.getInputStream();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];

        try {
            int bytesRead;
            while ((bytesRead = imageStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
            byte[] image = outputStream.toByteArray();

            Timestamp currentTime = new Timestamp(System.currentTimeMillis());

            Product product = new Product(name, description, price, sellerID, contactMethod, hiddenContent, currentTime, currentTime, image);

            if (DAOproduct.insertNewProduct(product)) {
                // Product inserted successfully

                response.sendRedirect("manage"); // Redirect to home page or any other page
            } else {
                // Product insertion failed
                response.sendRedirect("error.jsp"); // Redirect to error page
            }
        } catch (Exception ex) {
            // Xử lý ngoại lệ
            ex.printStackTrace(); // Hoặc xử lý ngoại lệ theo cách thích hợp
        } finally {
            // Đóng luồng và giải phóng tài nguyên
            if (imageStream != null) {
                try {
                    imageStream.close();
                } catch (IOException e) {
                    e.printStackTrace(); // Hoặc xử lý ngoại lệ theo cách thích hợp
                }
            }
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace(); // Hoặc xử lý ngoại lệ theo cách thích hợp
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
