<%@ page import="model.Product" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Đơn Bán của Bạn</title>
        <link rel="icon" type="image/png" href="images/logo.png">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/DonBan.css" rel="stylesheet" />
        <style>
            table{
                border-collapse: collapse
            }
            .content{
                margin: auto;
                width: 80%;
                float: left;
                margin-left: 30px;
                height: auto
            }
            .pagination {
                display: inline-block;
            }
            .pagination a {
                color: black;
                font-size: 22px;
                float: left;
                padding: 8px 16px;
                text-decoration: none;
            }
            .pagination a.active {
                background-color: #333;
                color: white;
            }
            .pagination a:hover:not(.active) {
                background-color: chocolate;
            </style>
        </head>
        <body>
            <header>
                <div class="header-left">

                </div>
                <div class="header-center">
                    <h1>GiCungBan</h1>

                </div>
                <div class="header-right">

                </div>
            </header>

            <c:if test="${sessionScope.acc != null}">
                <%-- Lấy sellerID từ sessionScope --%>
                <c:set var="sellerID" value="${sessionScope.acc.uid}" />
            </c:if>
            <nav>
                <a class="text-decoration-none" href="home">Publicly traded market</a>
                <a class="text-decoration-none" href="vnpay_pay.jsp">Payment management</a>
                <a class="text-decoration-none" href="manage">My sales order</a>
                <a class="text-decoration-none" href="DonMua">My purchase order</a>
                <c:if test="${sessionScope.acc !=null}" >
                    <a class="text-decoration-none" href="profile" class="">Hello ${sessionScope.acc.username}</a>
                    <a class="text-decoration-none"><fmt:formatNumber type="number" pattern="#,##0" value="${amount}"/> Đ</a>
                </c:if>
                <c:if test="${sessionScope.acc == null}" >
                    <a href="login.jsp" class="nav-item nav-link">Login</a>                               
                </c:if>
            </nav>

            <main>
                <div class="addbutton">
                    <button onclick="document.getElementById('addProductModal').style.display = 'block'">Add new product</button>
                </div>


                <!-- The Modal -->
                <div id="addProductModal" class="modal">
                    <div class="modal-content">
                        <span class="close" onclick="document.getElementById('addProductModal').style.display = 'none'">&times;</span>
                        <h2>Add new product</h2>
                        <h6 style="color: red">Intermediary fees: 5.000VND</h6>
                            <form action="createProductController" method="POST" enctype="multipart/form-data">
                                <label for="name">Product name:</label><br>
                                <input type="text" id="name" name="name" required><br>
                                <label for="description">Describe your product in detail (Public display):</label><br>
                                <textarea id="description" name="description" style="min-height: 200px;" required></textarea><br>
                            <label for="hiddenContent">Hidden content (Note: This part will be hidden and will only appear when the user has paid):</label><br>
                            <textarea id="hiddenContent" name="hiddenContent" style="min-height: 200px" required></textarea><br>
                                <label for="image">Product image:</label><br>
                                <input type="file" id="image" name="image" accept=".jpg, .jpeg, .png"><br>
                                <label for="price">Price of product: (enter number)</label><br>
                                <input type="number" id="price" name="price" required><br>
                                <label for="contactMethod">Contact method when making transactions: (FB , Zalo , Email , ...)</label><br>
                                <input type="text" id="contactMethod" name="contactMethod" required><br>
                                <button type="submit">Add product</button>
                            </form>
                        </div>
                    </div>

                    <h2>List of your products</h2>

                    <form action="searchBySellerId" method="post" class="search-form">
                        <input type="text" name="keyword" placeholder="Tìm kiếm tên sản phẩm bạn đang bán" class="search-input">
                        <input type="hidden" name="id" value="${sessionScope.acc.uid}">
                        <button type="submit" class="search-btn">Search</button>
                    </form>

                    <c:if test="${empty listC}">
                        <p style="color: red;">Product not found!</p>
                </c:if>
                        <br>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>ID product</th>
                            <th>Product name</th>
                            <th>Description</th>
                            <th>Image</th>
                            <th>Price</th>
                            <th>Contact method</th>                     
                            <th>Hidden content</th>
                            <th>Date created</th>
                            <th>Last updated</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="product" items="${listC}">
                            <tr>
                                <td>${product.id}</td>
                                <td>${product.name}</td>
                                <td>${product.description}</td>
                                <td>
                                    ${product.imageLink}
                                </td>

                                <td><fmt:formatNumber type="number" pattern="#,##0" value="${product.price}"/></td>
                                <td>${product.contactMethod}</td>                            
                                <td>${product.hiddenContent}</td>
                                <td>${product.createdAt}</td>
                                <td>${product.lastUpdated}</td>
                                <td>
                                    ${product.statusString}
                                    <c:if test="${product.statusString eq 'Reported'}">
                                        <button class="btn btn-warning" onclick="openModal(${product.id})">Execute</button>
                                    </c:if>
                                    <c:if test="${product.statusString eq 'Buyer request admin'}">
                                        <a onclick="return confirm('Are you sure? - Require 50.000 VND')" class="btn btn-primary" href="updateReport?status=request&id=${product.id}">Accept</a>
                                    </c:if>
                                    <c:if test="${product.statusString eq 'Processing'}">
                                        <a class="btn btn-primary" href="chat?id=${product.sellerID * 1000 + product.order.userID}">Contact</a>
                                    </c:if>
                                </td>

                                <td><a href="javascript:void(0);" class="btn" onclick="showUpdateForm('${product.id}', '${product.name}', '${product.description}',
                                                '${product.price}', '${product.contactMethod}', '${product.hiddenContent}', '${product.image}')">Update</a></td>
                                <td><a href="delete?pid=${product.id}" class="btn">Delete</a></td>
                            </tr>
                        </c:forEach>

                    </tbody>
                </table>
                <c:set var="page" value="${requestScope.page}"/>
                <div class="pagination">
                    <c:forEach begin="1" end="${requestScope.num}" var="i">
                        <a class="${i==page?"active":""}" href="manage?page=${i}">${i}</a>
                    </c:forEach>
                </div>


                <div id="updateProductModal" class="modal">
                    <div class="modal-content">
                        <span class="close" onclick="document.getElementById('updateProductModal').style.display = 'none'">&times;</span>
                        <h2>Update product</h2>
                        <form action="updateProductController" method="post" enctype="multipart/form-data">
                            <input type="hidden" id="updateId" name="id">
                            <label for="updateName">Product name:</label><br>
                            <input type="text" id="updateName" name="name" required><br>
                            <label for="updateDescription">Describe your product in detail (Public display):</label><br>   
                            <input type="text" id="updateDescription" name="description"required>
                            <label for="updateImage">Product image:</label><br>
                            <input type="file"  id="updateImage" name="image" accept=".jpg, .jpeg, .png"><br>
                            <label for="updatePrice">Product prrice:</label><br>
                            <input type="number" id="updatePrice" name="price" min="0" step="0.01" required><br>
                            <label for="updateContactMethod">Contact method when making transactions: (FB , Zalo , Email , ...)</label><br>
                            <input type="text" id="updateContactMethod" name="contactMethod" required><br>
                            <label for="updateHiddenContent">Hidden content (Note: This part will be hidden and will only appear when the user has paid):</label><br>
                            <input type="text" id="updateHiddenContent" name="hiddenContent" required><br>
                            <input type="submit" value="Cập nhật">
                        </form>
                    </div>
                </div>
                <script>
                    function showUpdateForm(id, name, description, price, contactMethod, hiddenContent, image) {
                        // Đặt giá trị của các trường vào các input trong form cập nhật
                        document.getElementById('updateId').value = id;
                        document.getElementById('updateName').value = name;
                        document.getElementById('updateDescription').value = description;
                        document.getElementById('updatePrice').value = price;
                        document.getElementById('updateContactMethod').value = contactMethod;
                        document.getElementById('updateHiddenContent').value = hiddenContent;
                        document.getElementById('updateImage').src = image;

                        // Hiển thị modal cập nhật
                        document.getElementById('updateProductModal').style.display = 'block';
                    }

                    function openModal(productId) {
                        document.getElementById('myModal').style.display = 'block';
                        document.getElementById('productId').innerText = productId;

                        document.getElementById('correctReport').href = 'updateReport?id=' + productId + '&status=correct';
                        document.getElementById('wrongReport').href = 'updateReport?id=' + productId + '&status=wrong';
                        document.getElementById('requestAdmin').href = 'updateReport?id=' + productId + '&status=seller-request';
                    }

                    function closeModal() {
                        document.getElementById('myModal').style.display = 'none';
                    }</script>

            </main>
            <footer class="footer text-center">
                <div class="footer-content">
                    <h3>Trang web Gi Cung Ban</h3>
                    <a>Điện thoại: +84 0234 567 89</a>
                    <a>Email: quangbz9512@gmail.com</a>
                    <a>Địa chỉ: 123 Đường Lac Trung, Phường Hai Ba Trung, Thành phố Ha Noi, Việt Nam</a>
                </div>
            </footer>

            <div id="myModal" class="modal">
                <div class="modal-content">
                    <span class="close" onclick="closeModal()">&times;</span>
                    <h2>Options for Product ID: <span id="productId"></span></h2>
                    <a class="btn btn-primary" id="correctReport" href="#">Correct Report</a>
                    <br><br>
                    <a class="btn btn-primary" id="wrongReport" href="#">Wrong Report</a>
                    <br><br>
                    <a class="btn btn-primary" id="requestAdmin" href="#">Request Admin</a>
                </div>
            </div>
        </body>
    </html>
