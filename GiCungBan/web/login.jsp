
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content=""/>
        <meta name="author" content=""/>
        <title>GiCungBan</title>
        <!-- Favicon -->
        <link rel="icon" type="image/x-icon" href="./images/logo.png"/>
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/LoginRegister.css" rel="stylesheet" />


    <body>
        <section class="vh-100" style="background-image: url('./images/back.jpg');">
            <div class="container py-5 h-100">
                <div class="row d-flex justify-content-center align-items-center h-100">
                    <div class="col col-xl-10">
                        <div class="card" style="border-radius: 1rem;">
                            <div class="row g-0">
                                <div class="col-md-6 col-lg-5 d-none d-md-block">
                                    <img
                                        src="./images/login2.jpg"
                                        alt="login form"
                                        class="img-fluid" style="border-radius: 1rem 0 0 1rem;"
                                        />
                                </div>

                                <!-- Your login form fields here -->
                                <div class="col-md-6 col-lg-7 d-flex align-items-center">
                                    <div class="card-body p-4 p-lg-5 text-black">
                                        <form action="login"  method="post">
                                            <div class="d-flex align-items-center mb-3 pb-1">
                                                <span class="h1 fw-bold mb-0">Login</span>
                                            </div>
                                            <div class="mb-1" style="color: green">${registered}</div>
                                            <h5 class="fw-normal pb-3" style="letter-spacing: 1px;">Sign into your account</h5>
                                            <div class="mb-2">
                                                <label class="form-label" for="form2Example17">Email address</label>
                                                <input type="email" id="form2Example17" class="form-control form-control-lg" name="email" value="${requestScope.email}"/>
                                            </div>                                           
                                            <div class="mb-2">
                                                <label class="form-label" for="form2Example27">Password</label>
                                                <input type="password" id="form2Example27" class="form-control form-control-lg" name="password"/>
                                            </div>
                                            <!-- Checkbox -->
                                            <div class="form-check d-flex justify-content-start mb-2">
                                                <input class="form-check-input me-2" type="checkbox" name="remember" value="true" id="form1Example3"/>
                                                <label class="form-check-label" for="form1Example3"> Remember me </label>                                              
                                            </div>
                                            
                                            <div class="mb-1" style="color: red">${mess}</div>
                                            <div class="mb-1" style="color: green">${sucess}</div>
                                            <div class="mb-1" style="color: green">${sucess2}</div>
                                            <img src="login" alt="Captcha Image">
                                            <input type="text" name="captchaInput" id="captchaInput">
                                            <div style="color: red">${requestScope.mess2}</div>
                                            <div class="mb-1" style="color: red">${notfound}</div>
                                            <div class="pt-1 mb-4">
                                                <button class="btn btn-dark btn-lg" type="submit">Login</button>
                                            </div>



                                            <p class="mb-3 pb-lg-2" style="color: #393f81;">
                                                Don't have an account?
                                            <a href="registration.jsp" style="color: #393f81;"><span style="font-weight: bold; color: black">Register here</span></a>
                                            </p>
                                            <p class="mb-3 pb-lg-2" style="color: #393f81;">
                                            <a href="forgotPass.jsp" style="color: #393f81;"><span style="font-weight: bold; color: black">Forgot password?</span></a>
                                            </p>
                                        </form>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>
