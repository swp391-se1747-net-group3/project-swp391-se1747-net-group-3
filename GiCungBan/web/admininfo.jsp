<%-- 
    Document   : admin
    Created on : Oct 26, 2023, 6:58:11 PM
    Author     : PC
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>

        <title>Admin-Information</title>
        <link rel="icon" type="image/png" href="images/logo.png">
        <link href="css/admin-styles.css" rel="stylesheet" />
        <link href="css/Home.css" rel="stylesheet" />
        <link rel="stylesheet" href="css/styles.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="sb-nav-fixed">
        <!-- Admin navbar -->
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <!-- Navbar Brand-->
            <a class="navbar-brand ps-3" href="adminHomeController"><i class="bi bi-briefcase"></i> Welcome, Admin !</a>          
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <a class="nav-link" href="adminHomeController">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Admin Home Manager
                            </a>
  
                        </div>
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <!-- Personal Session -->
                    <div class="container mt-5 mb-5">
                        <h1 class="pb-3 default-cursor">Welcome, <span class="text-warning">Trần Như</span> !</h1>
                        <section>
                            <div class="container">
                                <div class="row d-flex justify-content-center align-items-center h-100">
                                    <div class="col-12 col-md-9 col-lg-7 col-xl-6 mb-4">
                                        <h3 class="text-uppercase text-center mb-4 default-cursor">
                                            Your information
                                        </h3>
                                        <div class="mb-3 fw-bold">
                                            Full name: <span class="fw-lighter">Trần Như</span>
                                        </div>
                                        <div class="mb-3 fw-bold">
                                            Email: <span class="fw-lighter">lfsmarch18@gmail.com</span>
                                        </div>
                                        <div class="mb-3 fw-bold">
                                            Phone: <span class="fw-lighter">0372698544</span>
                                        </div>
                                        <c:choose>
                                            <c:when test="${not empty requestScope.MSG_SUCCESS}">
                                                <div class="alert alert-success mt-4" role="alert">
                                                    ${requestScope.MSG_SUCCESS}
                                                </div>
                                            </c:when>
                                            <c:when test="${not empty requestScope.MSG_ERROR}">
                                                <div class="alert alert-danger mt-4" role="alert">
                                                    ${requestScope.MSG_ERROR}
                                                </div>
                                            </c:when>
                                        </c:choose>
                                        <a href="logout"><button type="button" class="btn btn-default">Logout</button></a>
                                    </div>
                                </div>
                            </div>
                        </section>
                        
                    </div>
                </main>
                <!-- Footer -->
            </div>
        </div>       
    </body>
</html>
