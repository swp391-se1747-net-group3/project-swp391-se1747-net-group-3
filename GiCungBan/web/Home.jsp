<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>GiCungBan</title>
        <link rel="icon" type="image/png" href="images/logo.png">
        <link href="css/Home.css" rel="stylesheet" />
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
        <style>
            table{
                border-collapse: collapse
            }
            .content{
                margin: auto;
                width: 80%;
                float: left;
                margin-left: 30px;
                height: auto
            }
            .pagination {
                display: inline-block;
            }
            .pagination a {
                color: black;
                font-size: 22px;
                float: left;
                padding: 8px 16px;
                text-decoration: none;
            }
            .pagination a.active {
                background-color: #333;
                color: white;
            }
            .pagination a:hover:not(.active) {
                background-color: chocolate;
            </style>
        </head>
        <body>

            <!-- Header -->
            <header>
                <div class="header-left">

                </div>
                <div class="header-center">
                    <h1>GiCungBan</h1>
                </div>
                <div class="header-right">


                </div>
            </header>

            <!-- Navigation -->
            <nav>
                <a class="text-decoration-none" href="home">Publicly traded market</a>
                <a class="text-decoration-none" href="vnpay_pay.jsp">Payment management</a>
                <a class="text-decoration-none" href="manage">My sales order</a>
                <a class="text-decoration-none" href="DonMua">My purchase order</a>
                <c:if test="${sessionScope.acc !=null}" >
                    <a href="profile" class="">Hello ${sessionScope.acc.username}</a>
                    <a class="text-decoration-none"><fmt:formatNumber type="number" pattern="#,##0" value="${amount}"/> VND</a>
                    <button class="btn" onclick="openNotificationModal()">🔔</button>
                </c:if>
                <c:if test="${sessionScope.acc == null}" >
                    <a href="login.jsp" class="nav-item nav-link">Đăng nhập</a>                               
                </c:if>
            </nav>

            <!-- Main content -->
            <main>
                <div class="container-fluid mt-4">
                    <div class="home mb-4">
                        <div class="home-product">
                            <h2>List of products being sold as intermediaries </h2>
                            <div class="d-flex justify-content-between">

                <form action="search" method="post" class="search-form">
                    <input type="text" name="keyword" placeholder="Tìm kiếm tên sản phẩm" class="search-input">
                    <button type="submit" class="search-btn">Search</button>
                </form>
                <c:if test="${empty listP}">
                    <p style="color: red;">Product not found!</p>

                </c:if>
                <div class="forms-container">
                    <form action="sort" method="get" class="sort-form1">
                        <select name="sapxep" onchange="this.form.submit()">
                            <option value="default">Sort by price</option>
                            <option value="asc">Price increases</option>
                            <option value="desc">Price decreases</option>
                        </select>
                    </form>
                    <form action="sortbyname" method="get" class="sort-form2">
                        <select name="sapxepname" onchange="this.form.submit()">
                            <option value="default">Sort by name</option>
                            <option value="asc">A -> Z</option>
                            <option value="giam">Z -> A</option>
                        </select>
                    </form>
                </div>
                            </div>
                            <br>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>ID product</th>
                            <th>Product name</th>
                            <th>Price</th>
                            <th>Contact method</th>
                            <th>Seller name</th>
                            <th>Date created</th>
                            <th>Last updated</th>
                            <th colspan="2"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="product" items="${listP}">
                            <c:if test="${product.order eq null}">
                                <tr>
                                    <td>${product.id}</td>
                                    <td>${product.name}</td>
                                    <td><fmt:formatNumber type="number" pattern="#,##0" value="${product.price}"/> Đ</td>
                                    <td>${product.contactMethod}</td>
                                    <td>${product.username}</td>
                                    <td>${product.createdAt}</td>
                                    <td>${product.lastUpdated}</td>
                                    <td><a href="detail?pid=${product.id}" class="btn">Detail</a></td>

                                </tr>       
                            </c:if>
                        </c:forEach>
                    </tbody>
                </table>
                <c:set var="page" value="${requestScope.page}"/>
                <div class="pagination">
                    <c:forEach begin="1" end="${requestScope.num}" var="i">
                        <a class="${i==page?"active":""}" href="home?page=${i}">${i}</a>
                    </c:forEach>
                </div>
            </main>  


            <footer class="footer text-center">
                <div class="footer-content">
                    <h3>GiCungBan</h3>
                    <a>Phone: +84 0234 567 89</a>
                    <a>Email: gicungban24@gmail.com</a>
                    <a>Address: 123 Đường Lac Trung, Phường Hai Ba Trung, Thành phố Ha Noi, Việt Nam</a>
                </div>
            </footer>

            <div class="modal fade" id="notificationModal" tabindex="-1" role="dialog" aria-labelledby="notificationModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="notificationModalLabel">Notification</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <!-- Iterate over notifications and display content -->
                            <c:forEach var="notification" items="${notifications}">
                                <p>${notification.content}</p>
                            </c:forEach>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Bootstrap JS -->
                <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
                <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

                <!-- JavaScript to handle modal -->
                <script>
                            function openNotificationModal() {
                                $('#notificationModal').modal('show');
                            }

                            function closeNotificationModal() {
                                $('#notificationModal').modal('hide');
                            }
                </script>

        </body>
    </html>
