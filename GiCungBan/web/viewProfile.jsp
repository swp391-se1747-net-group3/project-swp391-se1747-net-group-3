<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>GiCungBan</title>
        <link rel="icon" type="image/png" href="images/logo.png">
        <!-- Bootstrap CSS link -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/Home.css" rel="stylesheet" />
        <link rel="icon" type="image/png" href="images/logo.png">
        <link href="css/DonBan.css" rel="stylesheet" />
        <style>
            table{
                border-collapse: collapse
            }
            .content{
                margin: auto;
                width: 80%;
                float: left;
                margin-left: 30px;
                height: auto
            }
            .pagination {
                display: inline-block;
            }
            .pagination a {
                color: black;
                font-size: 22px;
                float: left;
                padding: 8px 16px;
                text-decoration: none;
            }
            .pagination a.active {
                background-color: #333;
                color: white;
            }
            .pagination a:hover:not(.active) {
                background-color: chocolate;
            </style>
        </head>
        <body>

            <header>
                <div class="header-left">
                </div>
                <div class="header-center">
                    <h1>GiCungBan</h1>
                </div>
                <div class="header-right">
                </div>
            </header>

            <nav>
                <a class="text-decoration-none" href="home">Publicly traded market</a>
                <a class="text-decoration-none" href="vnpay_pay.jsp">Payment management</a>
                <a class="text-decoration-none" href="manage">My sales order</a>
                <a class="text-decoration-none" href="DonMua">My purchase order</a>
                <c:if test="${sessionScope.acc !=null}" >
                    <a href="profile" class="">Hello ${sessionScope.acc.username}</a>
                    <a class="text-decoration-none"><fmt:formatNumber type="number" pattern="#,##0" value="${amount}"/> VND</a>
                </c:if>
                <c:if test="${sessionScope.acc == null}" >
                    <a href="login.jsp" class="nav-item nav-link">Login</a>                               
                </c:if>
            </nav>

            <main>
                <div class="profile-container">
                    <div class="profile-info">
                        <h2>Seller information</h2>
                        <h5>Name: ${seller.username}</h5>
                        <h5>Email: ${seller.email}</h5>


                    </div>
                    <div class="product-list">
                        <h2>List of Products for Sale</h2>
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID product</th>
                                    <th>Product name</th>
                                    <th>Price</th>
                                    <th>Contact method</th>
                                    <th>Date created</th>
                                    <th>Last updated</th>
                                    <th>Detail product</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="product" items="${productList}">
                                    <tr>
                                        <td>${product.id}</td>
                                        <td>${product.name}</td>
                                        <td><fmt:formatNumber type="number" pattern="#,##0" value="${product.price}"/></td>
                                        <td>${product.contactMethod}</td>
                                        <td>${product.createdAt}</td>
                                        <td>${product.lastUpdated}</td>
                                        <td><a href="detail?pid=${product.id}" class="btn">Detail</a></td>

                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </main>

        </body>
        <footer class="footer text-center">
            <div class="footer-content">
                <h3>GiCungBan</h3>
                <a>Phone: +84 0234 567 89</a>
                <a>Email: gicungban24@gmail.com</a>
                <a>Address: 123 Đường Lac Trung, Phường Hai Ba Trung, Thành phố Ha Noi, Việt Nam</a>
            </div>
        </footer>
    </html>
