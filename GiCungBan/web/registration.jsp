

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content=""/>
        <meta name="author" content=""/>
        <title>Registration</title>


        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
        <link href="css/my-styles.css" rel="stylesheet">
        <!-- Show hide password -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
              integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://unpkg.com/bootstrap-show-password@1.2.1/dist/bootstrap-show-password.min.js"></script>
    </head>
    <body>
        <section class="h-100"
                 style="background-image: url('./images/BG2.jpg');">
            <div>
                <div class="container">
                    <div class="row d-flex justify-content-center align-items-center h-100">
                        <div class="col-12 col-md-9 col-lg-7 col-xl-6 pt-5 pb-5">
                            <div class="card" style="border-radius: 15px;">
                                <div class="card-body p-5">
                                    <h2 class="text-uppercase text-center mb-2">Create an account
                                    </h2>
                                    <form action="register" method="POST" id="register-form">
                                        <div class=" mb-2">
                                            <label class="form-label" for="register-input-psw">Your name</label>
                                            <input id="register-input-psw" class="form-control form-control-lg"
                                                   data-toggle="name" class="form-control" type="text" maxlength="50"
                                                   placeholder="Name" required
                                                   name="name">
                                        </div>

                                        <div class="mb-2">
                                            <label class="form-label" for="register-mail-input">Your Email</label>
                                            <input id="register-mail-input" type="text"
                                                   class="form-control form-control-lg" required
                                                   placeholder="example@gmail.com"
                                                   name="email" value="${requestScope.email}" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}">
                                        </div>


                                        <div class=" mb-2">
                                            <label class="form-label" for="register-input-phone">Phone number</label>
                                            <input type="text" id="register-input-phone" class="form-control form-control-lg"
                                                   required placeholder="0791234xx"
                                                   name="phone"/>
                                        </div>

                                        <div class=" mb-2">
                                            <label class="form-label" for="register-input-psw">Your address</label>
                                            <input id="register-input-psw" class="form-control form-control-lg"
                                                   data-toggle="address" class="form-control" type="text" maxlength="50"
                                                   placeholder="Address" required
                                                   name="address">
                                        </div>
                                        <!-- Password -->
                                        <div class=" mb-2">
                                            <label class="form-label" for="register-input-psw">Password</label>
                                            <input id="register-input-psw" class="form-control form-control-lg"
                                                   data-toggle="password" class="form-control" type="password" maxlength="50"
                                                   placeholder="Password" value="${sessionScope.pass}" required
                                                   name="password">
                                        </div>


                                        <!-- Confirm Password -->
                                        <div class="mb-2">
                                            <label class="form-label" for="register-input-rp-psw">Repeat your password</label>
                                            <input id="register-input-rp-psw" class="form-control form-control-lg"
                                                   data-toggle="password" class="form-control" type="password" maxlength="50"
                                                   placeholder="Repeat password" name="repassword" value="${sessionScope.repass}" required>
                                        </div>                                      

                                        <div class='text-danger ml-1'>${already}</div>
                                        <div class='text-danger ml-1'>${repassError}</div>

                                        <!-- Registion Btn -->
                                        <div class="d-flex justify-content-center pt-1">
                                            <button id="register-btn" type="submit"
                                                    class="btn btn-dark btn-lg">
                                                Register</button>
                                        </div>





                                        <p class="text-center text-muted mt-3 mb-0">
                                            Have already an account? 
                                            <a href="login.jsp" class="fw-bold text-body"><u>Login here</u></a></p>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>
